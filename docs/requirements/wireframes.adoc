= Wireframes: {project-name}
Richard Benndorf <s82003@htw-dresden.de>; Daria Bolotova <s82065@htw-dresden.de>; Tilman Ischner <s82044@htw-dresden.de>; Melanie Kadner <s81056@htw-dresden.de>; Alio Leuchtmann <s82067@htw-dresden.de>; Vincent Peters <s82627@htw-dresden.de>; Celine Wamser <s81090@htw-dresden.de>; Dominik Weber <s82021@htw-dresden.de>; Jordi Zmiani <s82071@htw-dresden.de>
{localdatetime}
include::../_includes/default-attributes.inc.adoc[]

Zur besseren Nachvollziehbarkeit der Use-Cases haben wir uns für eine visuelle Darstellung des Entwurfs in Form von Wireframes entschieden. Diese stellen eine grobe Übersicht für die Benutzeroberfläche dar und werden später noch detailliert.

=== *WF01*: Landing Page [[WF01]]

image::../Wireframes/wf01.jpg[title="Landing Page", align=center]

*Erklärung*:
Nutzer müssen sich auf der Landing Page authentifizieren, indem sie den aus dem  Dropdown-Menü "Account" den Knopf "Sign In" auswählen.

=== *WF02*: Login Prompt [[WF02]]

image::../Wireframes/wf02.jpg[title="Login Prompt", align=center]

*Erklärung*:
Der Login Prompt vordert den Nutzer auf seinen Login und sein Passwort einzutragen. Beim Klicken des "Sign in" Knopfes (und richtigen Zugangsdaten) wird der Nutzer auf die Homepage weitergeleitet.

=== *WF03*: Homepage [[WF03]]

image::../Wireframes/wf03.jpg[title="Homepage", align=center]

*Erklärung*:
Abhängig von den eigenen Rechten hat der Nutzer direkt Zugriff auf die einzelnen Datenbanktabellen.
// über das Dropdown-Menü "Datenbanktabellen".

=== *WF04*: Anzeigen der Datensätze einer bestimmten Datenbanktabellen [[WF04]]

image::../Wireframes/wf04.jpg[title="Anzeigen der Datensätze einer bestimmten Datenbanktabellen", align=center]

*Erklärung*:
In Abhängigkeit der jeweiligen Rechte des Nutzers oder nach Auswahl einer Datenbanktabelle werden die darin enthaltenen Datensätze angezeigt (hier exemplarisch für Tabelle Student) und erfüllen somit <<UC02,*UC02*>> und teils <<UC03*UC03*>>. Die Datensätze lassen sich über die Search-Bar durchsuchen oder mit einem Klick auf die Tabellenattribute filtern. Mit einem Klick auf den Button "Create new [Name der Datenbanktabelle]" ist es für einen berichtigen Nutzer möglich, die Subseite zum Anlegen eines Datensatzes zu öffnen.

=== *WF05*: Anlegen von Datensätzen [[WF05]]

image::../Wireframes/wf05.jpg[title="Subseite zum Anlegen eines Datensatzes", align=center]

*Erklärung*:
Nutzer kann benötigte Daten in die einzelnen Eingabefelder (hier exemplarisch für Tabelle Student) eingeben. Eingegebene Daten werden validiert und der Validierungsstatus am linken Rand der einzelnen Eingabefelder dargestellt. Dies erfüllt <<UC01,*UC01*>>. Das Verändern von Einträgen erfolgt über das selbe Schema nach Aufruf der Subseite zur Bearbeitung eines Eintrags (relevant in <<UC03,*UC03*>>).
