== Use-Case: Studentendaten anzeigen und suchen [[UC02]]

=== Kurzbeschreibung
Nach dem Importieren und Anlegen der Studentendaten kann der Student seine Daten jederzeit wieder abrufen, aber auch PA und die Dozenten können sich dessen Daten anzeigen lassen.

Anzeigen lassen von Studentendatensätzen in einer Listenansicht, wobei die angezeigten Datensätze anhand der Rollen (Student,Dozent,PA) unterteilt werden.

=== Kurzbeschreibung der Akteure

==== Studenten 
Ansehen der eigenen Daten.

==== Dozenten
Anzeigen der Studentendatensätze die von dem jeweiligen Dozenten Betreut werden. 

==== Praktikumsbeauftragter (PA)
Anzeigen von allen Studentendatensätzen.

=== Vorbedingungen

- Accounts müssen angelegt sein (Student/PA/Dozent)
- Nutzer muss mit Zugangsdaten angemeldet sein
//- klares Datenmodell (alle Informationen am richtigen Ort)
//- User Interface
//- (Richtigkeit der Daten)

=== Standardablauf (Basic Flow)

. Der Use Case beginnt sobald die Studentenübersicht ausgewählt wurde.
. Liste mit allen Studentendatensätzen erscheint die der jeweilige Nutzer sehen darf
. WHILE Nutzer unzufrieden mit angezeigten Datensätzen
.. Nutzer wählt Filteroptionen aus
.. Liste mit den gefilterten Studentendatensätzen erscheint
. IF Auswahl eines Studenten
.. Öffne Detail Ansicht des Studenten
. Der Use Case ist abgeschlossen.


//=== Alternative Abläufe
//Es gibt keine alternativen Abläufe. Die Art wie Studentendatensätze angezeigt werden ist für alle Aktuere gleich.

//=== Nachbedingungen
//Keine Nachbedingung, da das Ansehen von Datensätzen keine Veränderung des Systems bewirkt.

=== Besondere Anforderungen
==== Datenschutz
- Je nach Berechtigung bzw. Nutzerrolle müssen die richtigen Daten angezeigt werden
- Keine unbefugte Kopie möglich

==== Vollständigkeit
- Die Daten müssen vollständig angezeigt werden
- Die Daten müssen richtig formatiert angezeigt werden

//=== Platformunabhängigkeit
//- Die Anzeige muss auf allen üblichen Endgeräten responsive angepasst werden

=== Verweis auf Wireframes
Die genauere Implementierung des Use Cases wird in <<WF04,*WF04*>> behandelt.
