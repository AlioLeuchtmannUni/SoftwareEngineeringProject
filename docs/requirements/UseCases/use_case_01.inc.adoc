== Use-Case: Studentendaten importieren/anlegen [[UC01]]

=== Kurzbeschreibung
Manuelles Anlegen von Studentendatensätzen sowie Importieren von vorhandenen Studentendatensätzen.

=== Kurzbeschreibung der Akteure

==== Praktikumsbeauftragter (PA)
Der Praktikumsbeauftragte kann neue Studentendaten anlegen sowie importieren.

=== Vorbedingungen
- Account muss angelegt sein
- Nutzer muss mit Zugangsdaten angemeldet sein
- Angemeldeter Account muss die Rechte des Praktikumsverantwortlichen haben


=== Standardablauf (Basic Flow)
. Der Use Case beginnt, wenn die Subseite zum Anlegen eines Studentendatensatzes geöffnet wurde.
. WHILE Formular nicht vollständig
.. Nutzer wählt Eingabefeld aus
.. WHILE Validierung nicht korrekt
... Nutzer gibt Daten ein und System zeigt Validierungsstatus an (grün okay, rot falsch)
. Nutzer schließt Prozess
. IF System hat den Datensatz erfolgreich zur Datenbank hinzugefügt
.. Informiere Nutzer über Erfolg
. ELSE System konnte Datensatz nicht hinzufügen
.. Informiere Nutzer über Grund des Misserfolges
//. Eventuelles Routing zur Home Seite oder Eingabe eines neuen Datensatzes 
//(TO DO FRAGE AG)
. Der USE CASE ist abgeschlossen.

image::../images/uc01_activitydiagram.svg[title="Aktivitätsdiagramm für UC-01", align=center]

Das Aktivitätsdiagramm stellt den Standardablauf des Use-Cases dar. So wählt der Nutzer ebenfalls solange das Formular nicht vollständig ist Eingabefelder aus und gibt dort die Daten ein. Diese Daten werden anschließend validiert und sobald dies erfolgreich und das Formular vollständig ist, bestätigt der Nutzer die Eingabe indem er den Eingabeprozess schließt. Wenn das System den Datensatz erfolgreich zur Datenbank hinzugefügt hat, wird der Nutzer über den Erfolg benachrichtigt. Ist das Hinzufügen zur Datenbank nicht erfolgreich, so wird der Nutzer über den Misserfolg sowie über den Grund dafür informiert.

=== Alternative Abläufe

Alternativer Ablauf, wenn der Praktikumsverantwortliche die Daten importieren möchte.

. Der Use Case beginnt wenn die Subseite zum Anlegen eines Studentendatensatzes geöffnet wurde.
. Nutzer wählt den Import aus 
//(TO DO Datentyp erfragen (CSV/Datenbank/JSON ...))
. Das Formular wird automatisch gefüllt
. Fortsetzung des USE CASES bei BASIC FLOW Punkt 2.b

=== Nachbedingungen

==== Datenbank unverändert
- Nutzer hat die Eingabe verlassen
- Hinzufügen des Datensatzes in die Datenbank ist fehlgeschlagen

==== Datensatz verändert
- Use Case erfolgreich abgeschlossen, der Studentendatensatz wurde der Datenbank hinzugefügt

=== Besondere Anforderungen

==== Validierungskriterien

Die Attribute der Studentendatensätze unterliegen verschiedenen Validierungskriterien, welche bereits im <<#Datenmodell_Student,Datenmodell>> beschrieben wurden. Unter anderem auch eine Existenzprüfung von Matrikelnummer und sNummer.

==== Formatierung
- Daten müssen im deutschen Datumsformat einzugeben sein DD.MM.YYYY

==== Effizienz
- Der Praktikumsverantwortliche legt besonderen Wert auf die Effizienz, i.d.F. bedeutet dies, das die Datensätze schnellstmöglich angelegt werden können.

=== Verweis auf Wireframes
Die genauere Implementierung des Use Cases wird in <<WF05,*WF05*>> behandelt.

