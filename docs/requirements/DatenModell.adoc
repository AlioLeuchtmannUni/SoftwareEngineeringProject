= Datenmodell: {project-name}
Richard Benndorf <s82003@htw-dresden.de>; Daria Bolotova <s82065@htw-dresden.de>; Tilman Ischner <s82044@htw-dresden.de>; Melanie Kadner <s81056@htw-dresden.de>; Alio Leuchtmann <s82067@htw-dresden.de>; Vincent Peters <s82627@htw-dresden.de>; Celine Wamser <s81090@htw-dresden.de>; Dominik Weber <s82021@htw-dresden.de>; Jordi Zmiani <s82071@htw-dresden.de>
{localdatetime}
include::../_includes/default-attributes.inc.adoc[]


Format:  DataType name options validation

[cols="1,1"]
|===
| Symbol | Meaning

|u       
|unique 
|nn      
|not null 
|(...)   
|validierungskriterium 
|(alpha) 
|nur buchstaben 
|(FK)    
|Foreign Key 
|(validMn) 
|5 Zahlen ? 
|(validSn) 
|s+5 zahlen ? 
|(validSg) 
|(2(num) >curr-12) / (valid SGangNmr (3num)) / Grp(2num) 
|(validPrakDate) 
|Pruefe Zeitraum auf zukunft und gueltige laenge 
|(vorbedingungen) 
|noch zu pruefen wann welcher state eintreten kann beispiel: annerkennung nicht moeglich vor Erfolg
|(email) 
|fertige Validierung vorhanden
|(adresse) 
|fertige Validierung vorhanden
|===

[#Datenmodell_Student]
== Student

[cols="1,1,1,1",id=Datenmodell_Student]]
|===
| Datatype | Name | options | validation
|string| MatrikelNr| u nn| (validMn) 
|string| s-Nr| u nn| (validSn) 
|string| StudienGr| nn| (validSg) 
|string| Nachname| nn| (alpha) 
|string| Vorname| nn| (alpha) 
|  enum| Bachelor/Master/Diplom| nn|
|string| Notes| |(alpha) 
|===

== Dozent

[cols="1,1,1,1"]
|===
|string| Nachname| nn| (alpha) +
|string|  Vorname| nn| (alpha) +
|string|    email| nn| (email) +
|string|    Notes|   | (alpha) +
|===
== Unternehmen

[cols="1,1,1,1"]
|===
|string| Name| nn| (alpha) +
|string| Anschrift| nn| (validAdressFormat) +
|string| Branche| |(alpha) + 
|string| Notes| |(alpha) +
|===

== Vertrag 

[cols="1,1,1,1"]
|===
|Praktikant| Praktikant(Student.snummer)| nn| (FK) +
|Unternehmen| Unternehmen(Unternehmensname)| nn| (FK) +
|string| AnsprechpartnerUnternehmen| nn| (alpha) +
|string| PraktikumsOrt| nn| (alpha) +
|Dozent| Betreuer(Dozent.nachname)| nn| (FK) +
|Date| beginDatum| nn| (validPrakDate) +
|Date| endDatum| nn| (validPrakDate) +
|string| Notes| |(alpha) +
|boolean| berichtUndZeugnisVorhanden| |(vorbedingungen) +
|boolean| annerkennungErfolgt| |(vorbedingungen) +
|boolean| ErfolgGemeldet| |(vorbedingungen) +
|===



