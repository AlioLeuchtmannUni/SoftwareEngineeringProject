= System-Wide Requirements: {project-name}
Richard Benndorf <s82003@htw-dresden.de>; Daria Bolotova <s82065@htw-dresden.de>; Tilman Ischner <s82044@htw-dresden.de>; Melanie Kadner <s81056@htw-dresden.de>; Alio Leuchtmann <s82067@htw-dresden.de>; Vincent Peters <s82627@htw-dresden.de>; Celine Wamser <s81090@htw-dresden.de>; Dominik Weber <s82021@htw-dresden.de>; Jordi Zmiani <s82071@htw-dresden.de>
{localdatetime}
include::../_includes/default-attributes.inc.adoc[]
// Platzhalter für weitere Dokumenten-Attribute


== Einführung
In diesem Dokument werden die systemweiten Anforderungen für das Projekt Praktikumsverwaltung spezifiziert. Die Gliederung erfolgt nach der FURPS+ Anforderungsklassifikation:

* Systemweite funktionale Anforderungen (F),
* Qualitätsanforderungen für Benutzbarkeit, Zuverlässigkeit, Effizienz und Wartbarkeit (URPS) sowie
* zusätzliche Anforderungen (+) für technische, rechtliche, organisatorische Randbedingungen

NOTE: Die funktionalen Anforderungen, die sich aus der Interaktion von Nutzern mit dem System ergeben, sind als Use Cases in einem separaten Dokument festgehalten.




== Systemweite funktionale Anforderungen
//Angabe von system-weiten funktionalen Anforderungen, die nicht als Use Cases ausgedrückt werden können. Beispiele sind Drucken, Berichte, Authentifizierung, Änderungsverfolgung (Auditing), zeitgesteuerte Aktivitäten (Scheduling), Sicherheit / Maßnahmen zum Datenschutz
* *SWFA2:* Industrie üblicher Schutz der Webanwendung vor Zugriff auf Daten durch unbefugte.
* *SWFA4:* Zugriff auf bereits vorhandene HTW Datenbank (Eventuell TODO: klärung nötig)
* *SWFA5:* Protokollierung der Aktivitäten (Erstellen von Änderungsprotokolldateien)
* *SWFA6:* Versand von Erinnerungsmails (Eventuell TODO: klärung nötig)
* *SWFA7:* automatische Generierung von Bescheinigungen (mit und ohne Unterschrift)
* *SWFA8:* Verträge müssen in GUI angelegt werden können
* *SWFA9:* Autmatischer Versand von Event mails bei Status änderungen der Verträge (Eventuell TODO: klärung nötig)

== Qualitätsanforderungen für das Gesamtsystem
//Qualitätsanforderungen repräsentieren das "URPS" im FURPS+ zu Klassifikation von Anforderungen
* *SWQA1:* Code ist wiederverwendbar bzw. erweiterbar



=== Benutzbarkeit (Usability)
//Beschreiben Sie Anforderungen für Eigenschaften wie einfache Bedienung, einfaches Erlernen, Standards für die Benutzerfreundlichkeit, Lokalisierung (landesspezifische Anpassungen von Sprache, Datumsformaten, Währungen usw.)
* *SWUA1:* unterstützende Suche (Bsp. Vorschläge bei Namenssuche, passende  Verträge zu Person anzeigen)
* *SWUA3:* Einfaches Bedienen der Seite. (Orientierung an als positiv Empfundenen UIs und Feedback der Stakeholder)
* *SWUA4:* Responsive Design soll alle gängigen Viewport Größen unterstüten.
* *SWUA6:* Suchfunktionen für Studenten, Dozenten und Firmen (verschiedene Suchkriterien: Name etc.)

=== Zuverlässigkeit (Reliability)
// Zuverlässigkeit beinhaltet die Fähigkeit des Produkts und/oder des Systems unter Stress und ungünstigen Bedingungen am laufen zu bleiben. Spezifizieren Sie Anforderungen für zuverlässige Akzeptanzstufen, und wie diese gemessen und evaluiert werden. Vorgeschlagene Themen sind Verfügbarkeit, Häufigkeit und Schwere von Fehlern und Wiederherstellbarkeit.
* *SWZA1:* Uptime von 90%
* *SWZA2:*  Semantische Integrität der Datenbank: Beispiel: nicht 2 Studenten mit selber Matrikelnummer.
* *SWZA3:* Wartungsmodus (Fehlermeldung bei Versuch während Wartung sich einzuloggen)

=== Effizienz (Performance)
// Die Performanz Charakteristiken des Systems sollten in diesem Bereich ausgeführt werden. Beispiele sind Antwortzeit, Durchsatz, Kapazität und Zeiten zum Starten oder Beenden.
* *SWEA1:* Antwort/Ladezeiten der Webseite im Durchschnitt 3s

=== Wartbarkeit (Supportability)
// Dieser Bereich beschreibt sämtliche Anforderungen welche die Supportfähigkeit oder Wartbarkeit des zu entwickelnden Systems verbessern, einschließlich Anpassungsfähigkeit und Erweiterbarkeit, Kompatibilität, Skalierbarkeit und Anforderungen bezüglich der System Installation sowie Maß an Support und Wartbarkeit.
* *SWSA1:* Code erweiterbar (auch für später weiter entwickelbar)



== Zusätzliche Anforderungen

=== Einschränkungen

* zu unterstützende Brwoser: Mozilla Firefox, Google Chrome, Microsoft Edge, Internet Explorer, Safari, DuckDuckGo
* zu unterstützende Betriebssysteme: Windows, Linux, Mac (Client seitig), Serverseitig Linux Webserver der HTW Dresden
* Physische Begrenzungen für Hardware: Serverspezifikationen TODO: klärung nötig
* eventuell: LDAP Authentifizierung
* alle gängigen Betriebssysteme (Windows, Mac, Linux) unterstützen
//: ???
* Software in Sprache, welche der AG beherrscht (Java)
* kostenloses Framework

=== Organisatorische Randbedingungen
-keine-

=== Rechtliche Anforderungen

* Datenschutz der Personenbezogenen Daten muss gewährleistet werden
