= Iteration Plan 02: {project-name}
Richard Benndorf <s82003@htw-dresden.de>; Daria Bolotova <s82065@htw-dresden.de>; Tilman Ischner <s82044@htw-dresden.de>; Melanie Kadner <s81056@htw-dresden.de>; Alio Leuchtmann <s82067@htw-dresden.de>; Vincent Peters <s82627@htw-dresden.de>; Celine Wamser <s81090@htw-dresden.de>; Dominik Weber <s82021@htw-dresden.de>; Jordi Zmiani <s82071@htw-dresden.de>
{localdatetime}
include::../_includes/default-attributes.inc.adoc[]
// Platzhalter für weitere Dokumenten-Attribute


== Meilensteine
//Meilensteine zeigen den Ablauf der Iteration, wie z.B. den Beginn und das Ende, Zwischen-Meilensteine, Synchronisation mit anderen Teams, Demos usw.

zugehörig zu Meilenstein 2: LCA

[%header, cols="3,1"]
|===
| | Datum

| Beginn der Iteration | 13.12.2021
| Ende der Iteration | 18.12.2021
| |
|Meilenstein erreicht? |nein
|===


== Wesentliche Ziele
//Nennen Sie 1-5 wesentliche Ziele für die Iteration.

Genererierte Besipieldaten für Dozenten, Studenten und Firmen

* importieren
* anzeigen +

...können.


== Aufgabenzuordnung

==== Melanie und Celine
use-case-model 2

==== Alio

Tool aufsetzten, Datenmodell anpassen, Developement-DB fixen

==== Dominik, Tilman, Richard, Jordi
Datenmodell anpassen

==== Vincent, Daria
Beispieldaten erstellen +

==== Planung

Die in dieser Iteration geplanten Aufgaben sind in der Work Items List dargestellt
[[Git-Repos]]

image::../images/Iteration2.png[title="Screenshot Git-Repo Iteration 2, Stand 18.12.2021"]



== Probleme
-> siehe Risikomatrix

Das Problem in dieser Iteration war der Überschätzte Zeitaufwand für die Installation der Entwicklungsumgebung auf den PC's der Teammitglieder. Daraus ergaben sich erhebliche Zeitverzüge, wodruch ein einbinden der Beispieldaten nicht mehr möglich war. Auch vielen die adminsitrativen (Dokumentations)Aufgaben in dieser Iteration hinten runter.

Leider war die Motivation im Team für eben diese Dokumentation auch nicht sehr ausgeprägt, was sich im Feedback bemerkbar gemacht hat. Wichtiger war in dieser Iteration trotzdem erst einmal die Installation der Entwicklungsumgebung, da sonst nicht gewinnbringend weitergearbeitet werden kann. Trotzdem hätte uns mehr Engagement und besonders ein frühzeitiges Melden bei Fragen oder (Installations)Problemen viel Zeit gespart.


== Bewertungskriterien
//Eine kurze Beschreibung, wie Erfüllung die o.g. Ziele bewertet werden sollen.

* 80% der Aufgaben im Git-Repo sind in der Spalte "done".
* Das Einbinden und Anzeigen der Beispieldaten hat funkioniert.
* Das Einrichten der Entwicklungsumgebung hat auf verschiedenen Rechnern direkt oder nach zwei bis drei Versuchen geklappt.


== Assessment
//In diesem Abschnitt werden die Ergebnisse und Maßnahmen der Bewertung erfasst und kommuniziert. Die Bewertung wird üblicherweise am Ende jeder Iteration durchgeführt. Wenn Sie diese Bewertungen nicht machen, ist das Team möglicherweise nicht in der Lage, die eigene Arbeitsweise ("Way of Working") zu verbessern.

[%header, cols="1,3"]
|===
| Assessment Ziel | Review I02

| Assessment Datum | 18.12.2021
| Teilnehmer | Melanie Kadner, Richard Benndorf
| Projektstatus	| gelb
|===

* _Beurteilung im Vergleich zu den Zielen_

//Dokumentieren Sie, ob die angestrebten Ziele des Iterationsplans erreicht wurden.
Die angestrebten Ziele der Iteration wurden teilweise erreicht. Ein vollständiges Einbinden der Beispieldaten war nicht möglich, die Installation der Entwicklungsumgebung hat aber funktioniert. Nicht erledigte Aufgaben sind im Screenshot der <<Git-Repos>> zu erkennen. Zeitlich haben wir uns hier mit der Planung etwas überschätzt. 


* _Geplante vs. erledigte Aufgaben_

//Zusammenfassung, ob alle für die Iteration geplanten Aufgaben bearbeitet wurden und welche Aufgaben verschoben oder hinzugefügt wurden.
Fast alle geplanten Aufgaben wurden vollständig und fristgerecht erledigt. Das Testen und Einbinden der Beispieldatenbank sowie die Funktion zum Anzeigen derer hat zeitlich nicht mehr ganz in diese Iteration gepasst und wurde auf die Nächste verschoben. Das UML-Diagramm wurde erst einmal auf Eis gelegt.


* _Beurteilung im Vergleich zu den Bewertungskriterien_

//Document whether you met the evaluation criteria as specified in the Iteration Plan.
//Geben Sie an, ob Sie die o.g. Bewertungskriterien erfüllt haben. Das kann z.B. folgende Informationen enthalten: “Demo for Department X was well-received, with some concerns raised around usability,” or “495 test cases were automated with a 98% pass rate. 9 test cases were deferred because the corresponding Work Items were postponed.”

78% der Aufgaben im Git-Repo sind in der Spalte "done". Das sind knapp zu wenig. Erste Daten konnten jedoch angezeigt und bearbeitet werden, jedoch stammen diese nicht aus der ursprünglich angedachten Beispiel-Datenbank. Auch läuft die Software jetzt auf allen Rechnern nach max. drei Anläufen. Das vollständige Einbinden der Beispieldatenbank hat zeitlich nicht mehr funktioniert und muss in Iteration 3 nachgeholt werden.

=== ALPHA States - Essence Naviagtor

image::../images/Essence/Iteration2-essence.png[title= Screenshot Essence Navigator Iteration 2]
