package praktikumsverwaltung.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import praktikumsverwaltung.domain.Dozent;
import praktikumsverwaltung.repository.DozentRepository;
import praktikumsverwaltung.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link praktikumsverwaltung.domain.Dozent}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DozentResource {

    private final Logger log = LoggerFactory.getLogger(DozentResource.class);

    private static final String ENTITY_NAME = "dozent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DozentRepository dozentRepository;

    public DozentResource(DozentRepository dozentRepository) {
        this.dozentRepository = dozentRepository;
    }

    /**
     * {@code POST  /dozents} : Create a new dozent.
     *
     * @param dozent the dozent to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dozent, or with status {@code 400 (Bad Request)} if the dozent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dozents")
    public ResponseEntity<Dozent> createDozent(@Valid @RequestBody Dozent dozent) throws URISyntaxException {
        log.debug("REST request to save Dozent : {}", dozent);
        if (dozent.getId() != null) {
            throw new BadRequestAlertException("A new dozent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Dozent result = dozentRepository.save(dozent);
        return ResponseEntity
            .created(new URI("/api/dozents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dozents/:id} : Updates an existing dozent.
     *
     * @param id the id of the dozent to save.
     * @param dozent the dozent to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dozent,
     * or with status {@code 400 (Bad Request)} if the dozent is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dozent couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dozents/{id}")
    public ResponseEntity<Dozent> updateDozent(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Dozent dozent
    ) throws URISyntaxException {
        log.debug("REST request to update Dozent : {}, {}", id, dozent);
        if (dozent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dozent.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dozentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Dozent result = dozentRepository.save(dozent);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dozent.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /dozents/:id} : Partial updates given fields of an existing dozent, field will ignore if it is null
     *
     * @param id the id of the dozent to save.
     * @param dozent the dozent to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dozent,
     * or with status {@code 400 (Bad Request)} if the dozent is not valid,
     * or with status {@code 404 (Not Found)} if the dozent is not found,
     * or with status {@code 500 (Internal Server Error)} if the dozent couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/dozents/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Dozent> partialUpdateDozent(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Dozent dozent
    ) throws URISyntaxException {
        log.debug("REST request to partial update Dozent partially : {}, {}", id, dozent);
        if (dozent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dozent.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dozentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Dozent> result = dozentRepository
            .findById(dozent.getId())
            .map(
                existingDozent -> {
                    if (dozent.getNachname() != null) {
                        existingDozent.setNachname(dozent.getNachname());
                    }
                    if (dozent.getVorname() != null) {
                        existingDozent.setVorname(dozent.getVorname());
                    }
                    if (dozent.getEmail() != null) {
                        existingDozent.setEmail(dozent.getEmail());
                    }
                    if (dozent.getBeschreibung() != null) {
                        existingDozent.setBeschreibung(dozent.getBeschreibung());
                    }

                    return existingDozent;
                }
            )
            .map(dozentRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dozent.getId().toString())
        );
    }

    /**
     * {@code GET  /dozents} : get all the dozents.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dozents in body.
     */
    @GetMapping("/dozents")
    public List<Dozent> getAllDozents() {
        log.debug("REST request to get all Dozents");
        return dozentRepository.findAll();
    }

    /**
     * {@code GET  /dozents/:id} : get the "id" dozent.
     *
     * @param id the id of the dozent to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dozent, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dozents/{id}")
    public ResponseEntity<Dozent> getDozent(@PathVariable Long id) {
        log.debug("REST request to get Dozent : {}", id);
        Optional<Dozent> dozent = dozentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dozent);
    }

    /**
     * {@code DELETE  /dozents/:id} : delete the "id" dozent.
     *
     * @param id the id of the dozent to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dozents/{id}")
    public ResponseEntity<Void> deleteDozent(@PathVariable Long id) {
        log.debug("REST request to delete Dozent : {}", id);
        dozentRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
