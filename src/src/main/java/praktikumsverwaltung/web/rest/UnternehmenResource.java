package praktikumsverwaltung.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import praktikumsverwaltung.domain.Unternehmen;
import praktikumsverwaltung.repository.UnternehmenRepository;
import praktikumsverwaltung.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link praktikumsverwaltung.domain.Unternehmen}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class UnternehmenResource {

    private final Logger log = LoggerFactory.getLogger(UnternehmenResource.class);

    private static final String ENTITY_NAME = "unternehmen";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UnternehmenRepository unternehmenRepository;

    public UnternehmenResource(UnternehmenRepository unternehmenRepository) {
        this.unternehmenRepository = unternehmenRepository;
    }

    /**
     * {@code POST  /unternehmen} : Create a new unternehmen.
     *
     * @param unternehmen the unternehmen to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new unternehmen, or with status {@code 400 (Bad Request)} if the unternehmen has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/unternehmen")
    public ResponseEntity<Unternehmen> createUnternehmen(@Valid @RequestBody Unternehmen unternehmen) throws URISyntaxException {
        log.debug("REST request to save Unternehmen : {}", unternehmen);
        if (unternehmen.getId() != null) {
            throw new BadRequestAlertException("A new unternehmen cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Unternehmen result = unternehmenRepository.save(unternehmen);
        return ResponseEntity
            .created(new URI("/api/unternehmen/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /unternehmen/:id} : Updates an existing unternehmen.
     *
     * @param id the id of the unternehmen to save.
     * @param unternehmen the unternehmen to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated unternehmen,
     * or with status {@code 400 (Bad Request)} if the unternehmen is not valid,
     * or with status {@code 500 (Internal Server Error)} if the unternehmen couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/unternehmen/{id}")
    public ResponseEntity<Unternehmen> updateUnternehmen(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Unternehmen unternehmen
    ) throws URISyntaxException {
        log.debug("REST request to update Unternehmen : {}, {}", id, unternehmen);
        if (unternehmen.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, unternehmen.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!unternehmenRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Unternehmen result = unternehmenRepository.save(unternehmen);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, unternehmen.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /unternehmen/:id} : Partial updates given fields of an existing unternehmen, field will ignore if it is null
     *
     * @param id the id of the unternehmen to save.
     * @param unternehmen the unternehmen to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated unternehmen,
     * or with status {@code 400 (Bad Request)} if the unternehmen is not valid,
     * or with status {@code 404 (Not Found)} if the unternehmen is not found,
     * or with status {@code 500 (Internal Server Error)} if the unternehmen couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/unternehmen/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Unternehmen> partialUpdateUnternehmen(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Unternehmen unternehmen
    ) throws URISyntaxException {
        log.debug("REST request to partial update Unternehmen partially : {}, {}", id, unternehmen);
        if (unternehmen.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, unternehmen.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!unternehmenRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Unternehmen> result = unternehmenRepository
            .findById(unternehmen.getId())
            .map(
                existingUnternehmen -> {
                    if (unternehmen.getName() != null) {
                        existingUnternehmen.setName(unternehmen.getName());
                    }
                    if (unternehmen.getOrt() != null) {
                        existingUnternehmen.setOrt(unternehmen.getOrt());
                    }
                    if (unternehmen.getBranche() != null) {
                        existingUnternehmen.setBranche(unternehmen.getBranche());
                    }
                    if (unternehmen.getBeschreibung() != null) {
                        existingUnternehmen.setBeschreibung(unternehmen.getBeschreibung());
                    }
                    if (unternehmen.getPostleitzahl() != null) {
                        existingUnternehmen.setPostleitzahl(unternehmen.getPostleitzahl());
                    }
                    if (unternehmen.getAdresse() != null) {
                        existingUnternehmen.setAdresse(unternehmen.getAdresse());
                    }
                    if (unternehmen.getLand() != null) {
                        existingUnternehmen.setLand(unternehmen.getLand());
                    }

                    return existingUnternehmen;
                }
            )
            .map(unternehmenRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, unternehmen.getId().toString())
        );
    }

    /**
     * {@code GET  /unternehmen} : get all the unternehmen.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of unternehmen in body.
     */
    @GetMapping("/unternehmen")
    public List<Unternehmen> getAllUnternehmen() {
        log.debug("REST request to get all Unternehmen");
        return unternehmenRepository.findAll();
    }

    /**
     * {@code GET  /unternehmen/:id} : get the "id" unternehmen.
     *
     * @param id the id of the unternehmen to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the unternehmen, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/unternehmen/{id}")
    public ResponseEntity<Unternehmen> getUnternehmen(@PathVariable Long id) {
        log.debug("REST request to get Unternehmen : {}", id);
        Optional<Unternehmen> unternehmen = unternehmenRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(unternehmen);
    }

    /**
     * {@code DELETE  /unternehmen/:id} : delete the "id" unternehmen.
     *
     * @param id the id of the unternehmen to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/unternehmen/{id}")
    public ResponseEntity<Void> deleteUnternehmen(@PathVariable Long id) {
        log.debug("REST request to delete Unternehmen : {}", id);
        unternehmenRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
