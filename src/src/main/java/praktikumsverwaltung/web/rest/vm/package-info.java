/**
 * View Models used by Spring MVC REST controllers.
 */
package praktikumsverwaltung.web.rest.vm;
