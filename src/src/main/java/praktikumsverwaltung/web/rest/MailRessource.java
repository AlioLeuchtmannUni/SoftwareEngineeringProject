package praktikumsverwaltung.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import praktikumsverwaltung.domain.Student;
import praktikumsverwaltung.domain.Vertrag;
import praktikumsverwaltung.repository.VertragRepository;
import praktikumsverwaltung.service.MailService;
import praktikumsverwaltung.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class MailRessource {

    private String pbEmail = "alio.leuchtmann@gmx.de";
    private final Logger log = LoggerFactory.getLogger(AccountResource.class);
    private final MailService mailService;

    public MailRessource(MailService mailService) {
        this.mailService = mailService;
    }

    @PostMapping("/messageStudent")
    public void reportEmailToStudent(@Valid @RequestBody Vertrag vertrag) {
        System.out.println("Try to send to student");
        Student student = vertrag.getPraktikant();
        String studentFirstName = student.getVorname();
        String studentLastName = student.getNachname();
        String email = studentFirstName.toLowerCase() + "." + studentLastName.toLowerCase() + "@htw-dresden.de";
        System.out.println(email);
        mailService.sendEmail(email, "Praktikum erfolg gemeldet", erfolgGemeldetTemplate(studentFirstName, studentLastName), false, true);
    }

    @PostMapping("/messagePB")
    public void reportEmailToPB(@Valid @RequestBody Vertrag vertrag) {
        System.out.println("Try to send to pb");
        Student student = vertrag.getPraktikant();
        String studentFirstName = student.getVorname();
        String studentLastName = student.getNachname();
        String dozentName = vertrag.getDozent().getNachname();
        String bearbeitungsStatus = vertrag.getBearbeitungsStand().toString();
        mailService.sendEmail(
            pbEmail,
            "Praktikum Status Update",
            statusUpdateTemplate(dozentName, studentFirstName, studentLastName, bearbeitungsStatus),
            false,
            true
        );
    }

    private String erfolgGemeldetTemplate(String studentFirstname, String studentLastName) {
        return String.format(
            "<div>" +
            "<h2>Herzlichen Glückwunsch %s %s</h2>" +
            "<div>" +
            "Hiermit wird ihnen das Erfolgreiche bestehen ihres Praktikums mitgeteilt. <br> Mit freundlichen Grüßen ihre Praktikumsverwaltung." +
            "</div>" +
            "</div>",
            studentFirstname,
            studentLastName
        );
    }

    private String statusUpdateTemplate(String dozentName, String studentFirstName, String studentLastName, String bearbeitungsStatus) {
        return String.format(
            "<div>" +
            "<h2>Status änderung</h2>" +
            "<div>" +
            " %s hat den Status des Praktikumsvertrages von %s %s <br> auf %s geändert." +
            "</div>" +
            "</div>",
            dozentName,
            studentFirstName,
            studentLastName,
            bearbeitungsStatus
        );
    }
}
