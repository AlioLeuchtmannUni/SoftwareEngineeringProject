package praktikumsverwaltung.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import praktikumsverwaltung.domain.Vertrag;
import praktikumsverwaltung.repository.VertragRepository;
import praktikumsverwaltung.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link praktikumsverwaltung.domain.Vertrag}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class VertragResource {

    private final Logger log = LoggerFactory.getLogger(VertragResource.class);

    private static final String ENTITY_NAME = "vertrag";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VertragRepository vertragRepository;

    public VertragResource(VertragRepository vertragRepository) {
        this.vertragRepository = vertragRepository;
    }

    /**
     * {@code POST  /vertrags} : Create a new vertrag.
     *
     * @param vertrag the vertrag to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vertrag, or with status {@code 400 (Bad Request)} if the vertrag has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vertrags")
    public ResponseEntity<Vertrag> createVertrag(@Valid @RequestBody Vertrag vertrag) throws URISyntaxException {
        log.debug("REST request to save Vertrag : {}", vertrag);
        if (vertrag.getId() != null) {
            throw new BadRequestAlertException("A new vertrag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Vertrag result = vertragRepository.save(vertrag);
        return ResponseEntity
            .created(new URI("/api/vertrags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vertrags/:id} : Updates an existing vertrag.
     *
     * @param id the id of the vertrag to save.
     * @param vertrag the vertrag to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vertrag,
     * or with status {@code 400 (Bad Request)} if the vertrag is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vertrag couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vertrags/{id}")
    public ResponseEntity<Vertrag> updateVertrag(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Vertrag vertrag
    ) throws URISyntaxException {
        log.debug("REST request to update Vertrag : {}, {}", id, vertrag);
        if (vertrag.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vertrag.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vertragRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Vertrag result = vertragRepository.save(vertrag);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vertrag.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /vertrags/:id} : Partial updates given fields of an existing vertrag, field will ignore if it is null
     *
     * @param id the id of the vertrag to save.
     * @param vertrag the vertrag to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vertrag,
     * or with status {@code 400 (Bad Request)} if the vertrag is not valid,
     * or with status {@code 404 (Not Found)} if the vertrag is not found,
     * or with status {@code 500 (Internal Server Error)} if the vertrag couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/vertrags/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Vertrag> partialUpdateVertrag(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Vertrag vertrag
    ) throws URISyntaxException {
        log.debug("REST request to partial update Vertrag partially : {}, {}", id, vertrag);
        if (vertrag.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vertrag.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vertragRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Vertrag> result = vertragRepository
            .findById(vertrag.getId())
            .map(
                existingVertrag -> {
                    if (vertrag.getAnsprechpartner() != null) {
                        existingVertrag.setAnsprechpartner(vertrag.getAnsprechpartner());
                    }
                    if (vertrag.getBeginn() != null) {
                        existingVertrag.setBeginn(vertrag.getBeginn());
                    }
                    if (vertrag.getEnde() != null) {
                        existingVertrag.setEnde(vertrag.getEnde());
                    }
                    if (vertrag.getBearbeitungsStand() != null) {
                        existingVertrag.setBearbeitungsStand(vertrag.getBearbeitungsStand());
                    }
                    if (vertrag.getBeschreibung() != null) {
                        existingVertrag.setBeschreibung(vertrag.getBeschreibung());
                    }

                    return existingVertrag;
                }
            )
            .map(vertragRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vertrag.getId().toString())
        );
    }

    /**
     * {@code GET  /vertrags} : get all the vertrags.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vertrags in body.
     */
    @GetMapping("/vertrags")
    public List<Vertrag> getAllVertrags() {
        log.debug("REST request to get all Vertrags");
        return vertragRepository.findAll();
    }

    /**
     * {@code GET  /vertrags/:id} : get the "id" vertrag.
     *
     * @param id the id of the vertrag to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vertrag, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vertrags/{id}")
    public ResponseEntity<Vertrag> getVertrag(@PathVariable Long id) {
        log.debug("REST request to get Vertrag : {}", id);
        Optional<Vertrag> vertrag = vertragRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(vertrag);
    }

    /**
     * {@code DELETE  /vertrags/:id} : delete the "id" vertrag.
     *
     * @param id the id of the vertrag to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vertrags/{id}")
    public ResponseEntity<Void> deleteVertrag(@PathVariable Long id) {
        log.debug("REST request to delete Vertrag : {}", id);
        vertragRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
