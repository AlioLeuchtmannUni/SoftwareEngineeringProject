package praktikumsverwaltung.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import praktikumsverwaltung.domain.enumeration.BearbeitungsStand;

/**
 * A Vertrag.
 */
@Entity
@Table(name = "vertrag")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Vertrag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "ansprechpartner")
    private String ansprechpartner;

    @NotNull
    @Column(name = "beginn", nullable = false)
    private LocalDate beginn;

    @NotNull
    @Column(name = "ende", nullable = false)
    private LocalDate ende;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "bearbeitungs_stand", nullable = false)
    private BearbeitungsStand bearbeitungsStand;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "beschreibung")
    private String beschreibung;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Student praktikant;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "vertrags" }, allowSetters = true)
    private Dozent dozent;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "vertrags" }, allowSetters = true)
    private Unternehmen unternehmen;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vertrag id(Long id) {
        this.id = id;
        return this;
    }

    public String getAnsprechpartner() {
        return this.ansprechpartner;
    }

    public Vertrag ansprechpartner(String ansprechpartner) {
        this.ansprechpartner = ansprechpartner;
        return this;
    }

    public void setAnsprechpartner(String ansprechpartner) {
        this.ansprechpartner = ansprechpartner;
    }

    public LocalDate getBeginn() {
        return this.beginn;
    }

    public Vertrag beginn(LocalDate beginn) {
        this.beginn = beginn;
        return this;
    }

    public void setBeginn(LocalDate beginn) {
        this.beginn = beginn;
    }

    public LocalDate getEnde() {
        return this.ende;
    }

    public Vertrag ende(LocalDate ende) {
        this.ende = ende;
        return this;
    }

    public void setEnde(LocalDate ende) {
        this.ende = ende;
    }

    public BearbeitungsStand getBearbeitungsStand() {
        return this.bearbeitungsStand;
    }

    public Vertrag bearbeitungsStand(BearbeitungsStand bearbeitungsStand) {
        this.bearbeitungsStand = bearbeitungsStand;
        return this;
    }

    public void setBearbeitungsStand(BearbeitungsStand bearbeitungsStand) {
        this.bearbeitungsStand = bearbeitungsStand;
    }

    public String getBeschreibung() {
        return this.beschreibung;
    }

    public Vertrag beschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
        return this;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Student getPraktikant() {
        return this.praktikant;
    }

    public Vertrag praktikant(Student student) {
        this.setPraktikant(student);
        return this;
    }

    public void setPraktikant(Student student) {
        this.praktikant = student;
    }

    public Dozent getDozent() {
        return this.dozent;
    }

    public Vertrag dozent(Dozent dozent) {
        this.setDozent(dozent);
        return this;
    }

    public void setDozent(Dozent dozent) {
        this.dozent = dozent;
    }

    public Unternehmen getUnternehmen() {
        return this.unternehmen;
    }

    public Vertrag unternehmen(Unternehmen unternehmen) {
        this.setUnternehmen(unternehmen);
        return this;
    }

    public void setUnternehmen(Unternehmen unternehmen) {
        this.unternehmen = unternehmen;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vertrag)) {
            return false;
        }
        return id != null && id.equals(((Vertrag) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vertrag{" +
            "id=" + getId() +
            ", ansprechpartner='" + getAnsprechpartner() + "'" +
            ", beginn='" + getBeginn() + "'" +
            ", ende='" + getEnde() + "'" +
            ", bearbeitungsStand='" + getBearbeitungsStand() + "'" +
            ", beschreibung='" + getBeschreibung() + "'" +
            "}";
    }
}
