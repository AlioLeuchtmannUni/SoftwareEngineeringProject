package praktikumsverwaltung.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 * A Unternehmen.
 */
@Entity
@Table(name = "unternehmen")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Unternehmen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "ort", nullable = false)
    private String ort;

    @Column(name = "branche")
    private String branche;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "beschreibung")
    private String beschreibung;

    @NotNull
    @Column(name = "postleitzahl", nullable = false)
    private String postleitzahl;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    @NotNull
    @Column(name = "land", nullable = false)
    private String land;

    @OneToMany(mappedBy = "unternehmen")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "praktikant", "dozent", "unternehmen" }, allowSetters = true)
    private Set<Vertrag> vertrags = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Unternehmen id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Unternehmen name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrt() {
        return this.ort;
    }

    public Unternehmen ort(String ort) {
        this.ort = ort;
        return this;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getBranche() {
        return this.branche;
    }

    public Unternehmen branche(String branche) {
        this.branche = branche;
        return this;
    }

    public void setBranche(String branche) {
        this.branche = branche;
    }

    public String getBeschreibung() {
        return this.beschreibung;
    }

    public Unternehmen beschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
        return this;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getPostleitzahl() {
        return this.postleitzahl;
    }

    public Unternehmen postleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;

        return this;
    }

    public void setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Unternehmen adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getLand() {
        return this.land;
    }

    public Unternehmen land(String land) {
        this.land = land;
        return this;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public Set<Vertrag> getVertrags() {
        return this.vertrags;
    }

    public Unternehmen vertrags(Set<Vertrag> vertrags) {
        this.setVertrags(vertrags);
        return this;
    }

    public Unternehmen addVertrag(Vertrag vertrag) {
        this.vertrags.add(vertrag);
        vertrag.setUnternehmen(this);
        return this;
    }

    public Unternehmen removeVertrag(Vertrag vertrag) {
        this.vertrags.remove(vertrag);
        vertrag.setUnternehmen(null);
        return this;
    }

    public void setVertrags(Set<Vertrag> vertrags) {
        if (this.vertrags != null) {
            this.vertrags.forEach(i -> i.setUnternehmen(null));
        }
        if (vertrags != null) {
            vertrags.forEach(i -> i.setUnternehmen(this));
        }
        this.vertrags = vertrags;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Unternehmen)) {
            return false;
        }
        return id != null && id.equals(((Unternehmen) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Unternehmen{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ort='" + getOrt() + "'" +
            ", branche='" + getBranche() + "'" +
            ", beschreibung='" + getBeschreibung() + "'" +
            ", postleitzahl='" + getPostleitzahl() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", land='" + getLand() + "'" +
            "}";
    }
}
