package praktikumsverwaltung.domain.enumeration;

/**
 * The BearbeitungsStand enumeration.
 */
public enum BearbeitungsStand {
    VertragAngelegt,
    BerichtUndZeugnisVorhanden,
    AnnerkennungErfolgt,
    ErfolgGemeldet,
}
