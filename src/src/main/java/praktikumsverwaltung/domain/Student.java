package praktikumsverwaltung.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 * A Student.
 */
@Entity
@Table(name = "student")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "matrikel_nmr", nullable = false, unique = true)
    private String matrikelNmr;

    @NotNull
    @Column(name = "s_nummer", nullable = false, unique = true)
    private String sNummer;

    @NotNull
    @Column(name = "studien_gr", nullable = false)
    private String studienGr;

    @NotNull
    @Column(name = "nachname", nullable = false)
    private String nachname;

    @NotNull
    @Column(name = "vorname", nullable = false)
    private String vorname;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "beschreibung")
    private String beschreibung;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student id(Long id) {
        this.id = id;
        return this;
    }

    public String getMatrikelNmr() {
        return this.matrikelNmr;
    }

    public Student matrikelNmr(String matrikelNmr) {
        this.matrikelNmr = matrikelNmr;
        return this;
    }

    public void setMatrikelNmr(String matrikelNmr) {
        this.matrikelNmr = matrikelNmr;
    }

    public String getsNummer() {
        return this.sNummer;
    }

    public Student sNummer(String sNummer) {
        this.sNummer = sNummer;
        return this;
    }

    public void setsNummer(String sNummer) {
        this.sNummer = sNummer;
    }

    public String getStudienGr() {
        return this.studienGr;
    }

    public Student studienGr(String studienGr) {
        this.studienGr = studienGr;
        return this;
    }

    public void setStudienGr(String studienGr) {
        this.studienGr = studienGr;
    }

    public String getNachname() {
        return this.nachname;
    }

    public Student nachname(String nachname) {
        this.nachname = nachname;
        return this;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getVorname() {
        return this.vorname;
    }

    public Student vorname(String vorname) {
        this.vorname = vorname;
        return this;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getBeschreibung() {
        return this.beschreibung;
    }

    public Student beschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
        return this;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        return id != null && id.equals(((Student) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Student{" +
            "id=" + getId() +
            ", matrikelNmr='" + getMatrikelNmr() + "'" +
            ", sNummer='" + getsNummer() + "'" +
            ", studienGr='" + getStudienGr() + "'" +
            ", nachname='" + getNachname() + "'" +
            ", vorname='" + getVorname() + "'" +
            ", beschreibung='" + getBeschreibung() + "'" +
            "}";
    }
}
