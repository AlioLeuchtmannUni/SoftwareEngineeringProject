package praktikumsverwaltung.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 * A Dozent.
 */
@Entity
@Table(name = "dozent")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Dozent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nachname", nullable = false)
    private String nachname;

    @NotNull
    @Column(name = "vorname", nullable = false)
    private String vorname;

    @NotNull
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "beschreibung")
    private String beschreibung;

    @OneToMany(mappedBy = "dozent")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "praktikant", "dozent", "unternehmen" }, allowSetters = true)
    private Set<Vertrag> vertrags = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dozent id(Long id) {
        this.id = id;
        return this;
    }

    public String getNachname() {
        return this.nachname;
    }

    public Dozent nachname(String nachname) {
        this.nachname = nachname;
        return this;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getVorname() {
        return this.vorname;
    }

    public Dozent vorname(String vorname) {
        this.vorname = vorname;
        return this;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getEmail() {
        return this.email;
    }

    public Dozent email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBeschreibung() {
        return this.beschreibung;
    }

    public Dozent beschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
        return this;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Set<Vertrag> getVertrags() {
        return this.vertrags;
    }

    public Dozent vertrags(Set<Vertrag> vertrags) {
        this.setVertrags(vertrags);
        return this;
    }

    public Dozent addVertrag(Vertrag vertrag) {
        this.vertrags.add(vertrag);
        vertrag.setDozent(this);
        return this;
    }

    public Dozent removeVertrag(Vertrag vertrag) {
        this.vertrags.remove(vertrag);
        vertrag.setDozent(null);
        return this;
    }

    public void setVertrags(Set<Vertrag> vertrags) {
        if (this.vertrags != null) {
            this.vertrags.forEach(i -> i.setDozent(null));
        }
        if (vertrags != null) {
            vertrags.forEach(i -> i.setDozent(this));
        }
        this.vertrags = vertrags;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Dozent)) {
            return false;
        }
        return id != null && id.equals(((Dozent) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Dozent{" +
            "id=" + getId() +
            ", nachname='" + getNachname() + "'" +
            ", vorname='" + getVorname() + "'" +
            ", email='" + getEmail() + "'" +
            ", beschreibung='" + getBeschreibung() + "'" +
            "}";
    }
}
