package praktikumsverwaltung.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";


    // CUSTOM ROLES ADDED
    public static final String STUDENT = "ROLE_STUDENT";

    public static final String DOZENT = "ROLE_DOZENT";

    public static final String PRAKTIKUMSBEAUFTRAGTER = "ROLE_PRAKTIKUMSBEAUFTRAGTER";

    private AuthoritiesConstants() {}
}
