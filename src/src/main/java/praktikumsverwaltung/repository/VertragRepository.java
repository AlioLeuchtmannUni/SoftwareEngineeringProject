package praktikumsverwaltung.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import praktikumsverwaltung.domain.Vertrag;

/**
 * Spring Data SQL repository for the Vertrag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VertragRepository extends JpaRepository<Vertrag, Long> {}
