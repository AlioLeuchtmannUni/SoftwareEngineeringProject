package praktikumsverwaltung.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import praktikumsverwaltung.domain.Dozent;

/**
 * Spring Data SQL repository for the Dozent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DozentRepository extends JpaRepository<Dozent, Long> {}
