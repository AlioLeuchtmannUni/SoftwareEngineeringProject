package praktikumsverwaltung.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import praktikumsverwaltung.domain.Unternehmen;

/**
 * Spring Data SQL repository for the Unternehmen entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnternehmenRepository extends JpaRepository<Unternehmen, Long> {}
