import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { getVertragIdentifier, IVertrag } from 'app/entities/vertrag/vertrag.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createRequestOption } from 'app/core/request/request-util';
import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import * as dayjs from 'dayjs';
import { EntityArrayResponseType, EntityResponseType } from 'app/entities/vertrag/service/vertrag.service';

@Injectable({ providedIn: 'root' })
export class MailService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  reportErfolgGemeldetToStudent(vertrag: IVertrag): Observable<HttpResponse<string>> {
    const copy = this.convertDateFromClient(vertrag);
    return this.http.post<string>(this.resourceUrl + '/messageStudent', copy, { observe: 'response' });
  }

  reportStatusChangeToPraktikumsBeauftragtem(vertrag: IVertrag): Observable<HttpResponse<string>> {
    const copy = this.convertDateFromClient(vertrag);
    return this.http.post<string>(this.resourceUrl + '/messagePB', copy, { observe: 'response' });
  }
  protected convertDateFromClient(vertrag: IVertrag): IVertrag {
    return Object.assign({}, vertrag, {
      beginn: vertrag.beginn?.isValid() ? vertrag.beginn.format(DATE_FORMAT) : undefined,
      ende: vertrag.ende?.isValid() ? vertrag.ende.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.beginn = res.body.beginn ? dayjs(res.body.beginn) : undefined;
      res.body.ende = res.body.ende ? dayjs(res.body.ende) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((vertrag: IVertrag) => {
        vertrag.beginn = vertrag.beginn ? dayjs(vertrag.beginn) : undefined;
        vertrag.ende = vertrag.ende ? dayjs(vertrag.ende) : undefined;
      });
    }
    return res;
  }
}
