import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'student',
        data: { pageTitle: 'Students' },
        loadChildren: () => import('./student/student.module').then(m => m.StudentModule),
      },
      {
        path: 'dozent',
        data: { pageTitle: 'Dozents' },
        loadChildren: () => import('./dozent/dozent.module').then(m => m.DozentModule),
      },
      {
        path: 'unternehmen',
        data: { pageTitle: 'Unternehmen' },
        loadChildren: () => import('./unternehmen/unternehmen.module').then(m => m.UnternehmenModule),
      },
      {
        path: 'vertrag',
        data: { pageTitle: 'Vertrags' },
        loadChildren: () => import('./vertrag/vertrag.module').then(m => m.VertragModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
