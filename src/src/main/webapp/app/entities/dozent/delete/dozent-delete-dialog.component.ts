import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDozent } from '../dozent.model';
import { DozentService } from '../service/dozent.service';

@Component({
  templateUrl: './dozent-delete-dialog.component.html',
})
export class DozentDeleteDialogComponent {
  dozent?: IDozent;

  constructor(protected dozentService: DozentService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dozentService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
