jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { DozentService } from '../service/dozent.service';

import { DozentDeleteDialogComponent } from './dozent-delete-dialog.component';

describe('Component Tests', () => {
  describe('Dozent Management Delete Component', () => {
    let comp: DozentDeleteDialogComponent;
    let fixture: ComponentFixture<DozentDeleteDialogComponent>;
    let service: DozentService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DozentDeleteDialogComponent],
        providers: [NgbActiveModal],
      })
        .overrideTemplate(DozentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DozentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DozentService);
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.close).not.toHaveBeenCalled();
        expect(mockActiveModal.dismiss).toHaveBeenCalled();
      });
    });
  });
});
