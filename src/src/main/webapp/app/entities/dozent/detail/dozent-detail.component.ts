import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDozent } from '../dozent.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-dozent-detail',
  templateUrl: './dozent-detail.component.html',
})
export class DozentDetailComponent implements OnInit {
  dozent: IDozent | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dozent }) => {
      this.dozent = dozent;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
