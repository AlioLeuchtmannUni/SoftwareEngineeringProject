import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DozentService } from '../service/dozent.service';

import { DozentComponent } from './dozent.component';

describe('Component Tests', () => {
  describe('Dozent Management Component', () => {
    let comp: DozentComponent;
    let fixture: ComponentFixture<DozentComponent>;
    let service: DozentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DozentComponent],
      })
        .overrideTemplate(DozentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DozentComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DozentService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.dozents?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
