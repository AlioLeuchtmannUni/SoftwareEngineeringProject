import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDozent } from '../dozent.model';
import { DozentService } from '../service/dozent.service';
import { DozentDeleteDialogComponent } from '../delete/dozent-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-dozent',
  templateUrl: './dozent.component.html',
})
export class DozentComponent implements OnInit {
  dozents?: IDozent[];
  isLoading = false;

  constructor(protected dozentService: DozentService, protected dataUtils: DataUtils, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.dozentService.query().subscribe(
      (res: HttpResponse<IDozent[]>) => {
        this.isLoading = false;
        this.dozents = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDozent): number {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(dozent: IDozent): void {
    const modalRef = this.modalService.open(DozentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.dozent = dozent;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
