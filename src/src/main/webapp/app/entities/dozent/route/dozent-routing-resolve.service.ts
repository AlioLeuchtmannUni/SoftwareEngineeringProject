import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDozent, Dozent } from '../dozent.model';
import { DozentService } from '../service/dozent.service';

@Injectable({ providedIn: 'root' })
export class DozentRoutingResolveService implements Resolve<IDozent> {
  constructor(protected service: DozentService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDozent> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dozent: HttpResponse<Dozent>) => {
          if (dozent.body) {
            return of(dozent.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Dozent());
  }
}
