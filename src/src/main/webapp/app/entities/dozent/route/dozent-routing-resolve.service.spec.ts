jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDozent, Dozent } from '../dozent.model';
import { DozentService } from '../service/dozent.service';

import { DozentRoutingResolveService } from './dozent-routing-resolve.service';

describe('Service Tests', () => {
  describe('Dozent routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: DozentRoutingResolveService;
    let service: DozentService;
    let resultDozent: IDozent | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(DozentRoutingResolveService);
      service = TestBed.inject(DozentService);
      resultDozent = undefined;
    });

    describe('resolve', () => {
      it('should return IDozent returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDozent = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDozent).toEqual({ id: 123 });
      });

      it('should return new IDozent if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDozent = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultDozent).toEqual(new Dozent());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDozent = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDozent).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
