import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DozentComponent } from '../list/dozent.component';
import { DozentDetailComponent } from '../detail/dozent-detail.component';
import { DozentUpdateComponent } from '../update/dozent-update.component';
import { DozentRoutingResolveService } from './dozent-routing-resolve.service';

const dozentRoute: Routes = [
  {
    path: '',
    component: DozentComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DozentDetailComponent,
    resolve: {
      dozent: DozentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DozentUpdateComponent,
    resolve: {
      dozent: DozentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DozentUpdateComponent,
    resolve: {
      dozent: DozentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dozentRoute)],
  exports: [RouterModule],
})
export class DozentRoutingModule {}
