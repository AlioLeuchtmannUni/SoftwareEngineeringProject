import { IVertrag } from 'app/entities/vertrag/vertrag.model';

export interface IDozent {
  id?: number;
  nachname?: string;
  vorname?: string;
  email?: string;
  beschreibung?: string | null;
  vertrags?: IVertrag[] | null;
}

export class Dozent implements IDozent {
  constructor(
    public id?: number,
    public nachname?: string,
    public vorname?: string,
    public email?: string,
    public beschreibung?: string | null,
    public vertrags?: IVertrag[] | null
  ) {}
}

export function getDozentIdentifier(dozent: IDozent): number | undefined {
  return dozent.id;
}
