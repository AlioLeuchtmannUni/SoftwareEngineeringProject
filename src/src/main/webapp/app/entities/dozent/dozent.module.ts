import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { DozentComponent } from './list/dozent.component';
import { DozentDetailComponent } from './detail/dozent-detail.component';
import { DozentUpdateComponent } from './update/dozent-update.component';
import { DozentDeleteDialogComponent } from './delete/dozent-delete-dialog.component';
import { DozentRoutingModule } from './route/dozent-routing.module';

@NgModule({
  imports: [SharedModule, DozentRoutingModule],
  declarations: [DozentComponent, DozentDetailComponent, DozentUpdateComponent, DozentDeleteDialogComponent],
  entryComponents: [DozentDeleteDialogComponent],
})
export class DozentModule {}
