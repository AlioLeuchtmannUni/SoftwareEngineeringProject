import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IDozent, Dozent } from '../dozent.model';
import { DozentService } from '../service/dozent.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-dozent-update',
  templateUrl: './dozent-update.component.html',
})
export class DozentUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nachname: [null, [Validators.required,Validators.minLength(3),Validators.maxLength(50)]],
    vorname: [null, [Validators.required,Validators.minLength(3),Validators.maxLength(50)]],
    email: [null, [Validators.required,Validators.email]],
    beschreibung: [Validators.maxLength(200)],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected dozentService: DozentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dozent }) => {
      this.updateForm(dozent);
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('praktikumsverwaltungApp.error', { message: err.message })
        ),
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dozent = this.createFromForm();
    if (dozent.id !== undefined) {
      this.subscribeToSaveResponse(this.dozentService.update(dozent));
    } else {
      this.subscribeToSaveResponse(this.dozentService.create(dozent));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDozent>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dozent: IDozent): void {
    this.editForm.patchValue({
      id: dozent.id,
      nachname: dozent.nachname,
      vorname: dozent.vorname,
      email: dozent.email,
      beschreibung: dozent.beschreibung,
    });
  }

  protected createFromForm(): IDozent {
    return {
      ...new Dozent(),
      id: this.editForm.get(['id'])!.value,
      nachname: this.editForm.get(['nachname'])!.value,
      vorname: this.editForm.get(['vorname'])!.value,
      email: this.editForm.get(['email'])!.value,
      beschreibung: this.editForm.get(['beschreibung'])!.value,
    };
  }
}
