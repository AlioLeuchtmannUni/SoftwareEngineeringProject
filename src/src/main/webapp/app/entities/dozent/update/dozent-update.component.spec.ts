jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DozentService } from '../service/dozent.service';
import { IDozent, Dozent } from '../dozent.model';

import { DozentUpdateComponent } from './dozent-update.component';

describe('Component Tests', () => {
  describe('Dozent Management Update Component', () => {
    let comp: DozentUpdateComponent;
    let fixture: ComponentFixture<DozentUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dozentService: DozentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DozentUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DozentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DozentUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dozentService = TestBed.inject(DozentService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const dozent: IDozent = { id: 456 };

        activatedRoute.data = of({ dozent });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dozent));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dozent = { id: 123 };
        spyOn(dozentService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dozent });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dozent }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dozentService.update).toHaveBeenCalledWith(dozent);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dozent = new Dozent();
        spyOn(dozentService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dozent });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dozent }));
        saveSubject.complete();

        // THEN
        expect(dozentService.create).toHaveBeenCalledWith(dozent);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dozent = { id: 123 };
        spyOn(dozentService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dozent });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dozentService.update).toHaveBeenCalledWith(dozent);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
