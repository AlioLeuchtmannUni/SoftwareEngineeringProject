import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDozent, Dozent } from '../dozent.model';

import { DozentService } from './dozent.service';

describe('Service Tests', () => {
  describe('Dozent Service', () => {
    let service: DozentService;
    let httpMock: HttpTestingController;
    let elemDefault: IDozent;
    let expectedResult: IDozent | IDozent[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DozentService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        nachname: 'AAAAAAA',
        vorname: 'AAAAAAA',
        email: 'AAAAAAA',
        beschreibung: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Dozent', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Dozent()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Dozent', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            nachname: 'BBBBBB',
            vorname: 'BBBBBB',
            email: 'BBBBBB',
            beschreibung: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Dozent', () => {
        const patchObject = Object.assign(
          {
            beschreibung: 'BBBBBB',
          },
          new Dozent()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Dozent', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            nachname: 'BBBBBB',
            vorname: 'BBBBBB',
            email: 'BBBBBB',
            beschreibung: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Dozent', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDozentToCollectionIfMissing', () => {
        it('should add a Dozent to an empty array', () => {
          const dozent: IDozent = { id: 123 };
          expectedResult = service.addDozentToCollectionIfMissing([], dozent);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dozent);
        });

        it('should not add a Dozent to an array that contains it', () => {
          const dozent: IDozent = { id: 123 };
          const dozentCollection: IDozent[] = [
            {
              ...dozent,
            },
            { id: 456 },
          ];
          expectedResult = service.addDozentToCollectionIfMissing(dozentCollection, dozent);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Dozent to an array that doesn't contain it", () => {
          const dozent: IDozent = { id: 123 };
          const dozentCollection: IDozent[] = [{ id: 456 }];
          expectedResult = service.addDozentToCollectionIfMissing(dozentCollection, dozent);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dozent);
        });

        it('should add only unique Dozent to an array', () => {
          const dozentArray: IDozent[] = [{ id: 123 }, { id: 456 }, { id: 63217 }];
          const dozentCollection: IDozent[] = [{ id: 123 }];
          expectedResult = service.addDozentToCollectionIfMissing(dozentCollection, ...dozentArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dozent: IDozent = { id: 123 };
          const dozent2: IDozent = { id: 456 };
          expectedResult = service.addDozentToCollectionIfMissing([], dozent, dozent2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dozent);
          expect(expectedResult).toContain(dozent2);
        });

        it('should accept null and undefined values', () => {
          const dozent: IDozent = { id: 123 };
          expectedResult = service.addDozentToCollectionIfMissing([], null, dozent, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dozent);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
