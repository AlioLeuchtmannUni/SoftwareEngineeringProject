import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDozent, getDozentIdentifier } from '../dozent.model';

export type EntityResponseType = HttpResponse<IDozent>;
export type EntityArrayResponseType = HttpResponse<IDozent[]>;

@Injectable({ providedIn: 'root' })
export class DozentService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/dozents');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(dozent: IDozent): Observable<EntityResponseType> {
    return this.http.post<IDozent>(this.resourceUrl, dozent, { observe: 'response' });
  }

  update(dozent: IDozent): Observable<EntityResponseType> {
    return this.http.put<IDozent>(`${this.resourceUrl}/${getDozentIdentifier(dozent) as number}`, dozent, { observe: 'response' });
  }

  partialUpdate(dozent: IDozent): Observable<EntityResponseType> {
    return this.http.patch<IDozent>(`${this.resourceUrl}/${getDozentIdentifier(dozent) as number}`, dozent, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDozent>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDozent[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDozentToCollectionIfMissing(dozentCollection: IDozent[], ...dozentsToCheck: (IDozent | null | undefined)[]): IDozent[] {
    const dozents: IDozent[] = dozentsToCheck.filter(isPresent);
    if (dozents.length > 0) {
      const dozentCollectionIdentifiers = dozentCollection.map(dozentItem => getDozentIdentifier(dozentItem)!);
      const dozentsToAdd = dozents.filter(dozentItem => {
        const dozentIdentifier = getDozentIdentifier(dozentItem);
        if (dozentIdentifier == null || dozentCollectionIdentifiers.includes(dozentIdentifier)) {
          return false;
        }
        dozentCollectionIdentifiers.push(dozentIdentifier);
        return true;
      });
      return [...dozentsToAdd, ...dozentCollection];
    }
    return dozentCollection;
  }
}
