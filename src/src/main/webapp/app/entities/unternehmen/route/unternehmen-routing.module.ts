import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { UnternehmenComponent } from '../list/unternehmen.component';
import { UnternehmenDetailComponent } from '../detail/unternehmen-detail.component';
import { UnternehmenUpdateComponent } from '../update/unternehmen-update.component';
import { UnternehmenRoutingResolveService } from './unternehmen-routing-resolve.service';

const unternehmenRoute: Routes = [
  {
    path: '',
    component: UnternehmenComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UnternehmenDetailComponent,
    resolve: {
      unternehmen: UnternehmenRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UnternehmenUpdateComponent,
    resolve: {
      unternehmen: UnternehmenRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UnternehmenUpdateComponent,
    resolve: {
      unternehmen: UnternehmenRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(unternehmenRoute)],
  exports: [RouterModule],
})
export class UnternehmenRoutingModule {}
