import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IUnternehmen, Unternehmen } from '../unternehmen.model';
import { UnternehmenService } from '../service/unternehmen.service';

@Injectable({ providedIn: 'root' })
export class UnternehmenRoutingResolveService implements Resolve<IUnternehmen> {
  constructor(protected service: UnternehmenService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUnternehmen> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((unternehmen: HttpResponse<Unternehmen>) => {
          if (unternehmen.body) {
            return of(unternehmen.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Unternehmen());
  }
}
