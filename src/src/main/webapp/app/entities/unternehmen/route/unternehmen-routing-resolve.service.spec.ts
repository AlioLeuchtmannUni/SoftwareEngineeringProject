jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IUnternehmen, Unternehmen } from '../unternehmen.model';
import { UnternehmenService } from '../service/unternehmen.service';

import { UnternehmenRoutingResolveService } from './unternehmen-routing-resolve.service';

describe('Service Tests', () => {
  describe('Unternehmen routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: UnternehmenRoutingResolveService;
    let service: UnternehmenService;
    let resultUnternehmen: IUnternehmen | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(UnternehmenRoutingResolveService);
      service = TestBed.inject(UnternehmenService);
      resultUnternehmen = undefined;
    });

    describe('resolve', () => {
      it('should return IUnternehmen returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultUnternehmen = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultUnternehmen).toEqual({ id: 123 });
      });

      it('should return new IUnternehmen if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultUnternehmen = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultUnternehmen).toEqual(new Unternehmen());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultUnternehmen = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultUnternehmen).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
