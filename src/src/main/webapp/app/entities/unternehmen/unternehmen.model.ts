import { IVertrag } from 'app/entities/vertrag/vertrag.model';

export interface IUnternehmen {
  id?: number;
  name?: string;
  ort?: string;
  branche?: string | null;
  beschreibung?: string | null;
  postleitzahl?: string;
  adresse?: string;
  land?: string;
  vertrags?: IVertrag[] | null;
}

export class Unternehmen implements IUnternehmen {
  constructor(
    public id?: number,
    public name?: string,
    public ort?: string,
    public branche?: string | null,
    public beschreibung?: string | null,
    public postleitzahl?: string,
    public adresse?: string,
    public land?: string,
    public vertrags?: IVertrag[] | null
  ) {}
}

export function getUnternehmenIdentifier(unternehmen: IUnternehmen): number | undefined {
  return unternehmen.id;
}
