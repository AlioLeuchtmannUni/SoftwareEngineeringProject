jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { UnternehmenService } from '../service/unternehmen.service';
import { IUnternehmen, Unternehmen } from '../unternehmen.model';

import { UnternehmenUpdateComponent } from './unternehmen-update.component';

describe('Component Tests', () => {
  describe('Unternehmen Management Update Component', () => {
    let comp: UnternehmenUpdateComponent;
    let fixture: ComponentFixture<UnternehmenUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let unternehmenService: UnternehmenService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [UnternehmenUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(UnternehmenUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UnternehmenUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      unternehmenService = TestBed.inject(UnternehmenService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const unternehmen: IUnternehmen = { id: 456 };

        activatedRoute.data = of({ unternehmen });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(unternehmen));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const unternehmen = { id: 123 };
        spyOn(unternehmenService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ unternehmen });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: unternehmen }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(unternehmenService.update).toHaveBeenCalledWith(unternehmen);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const unternehmen = new Unternehmen();
        spyOn(unternehmenService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ unternehmen });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: unternehmen }));
        saveSubject.complete();

        // THEN
        expect(unternehmenService.create).toHaveBeenCalledWith(unternehmen);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const unternehmen = { id: 123 };
        spyOn(unternehmenService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ unternehmen });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(unternehmenService.update).toHaveBeenCalledWith(unternehmen);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
