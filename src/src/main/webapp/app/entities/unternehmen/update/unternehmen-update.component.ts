import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IUnternehmen, Unternehmen } from '../unternehmen.model';
import { UnternehmenService } from '../service/unternehmen.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-unternehmen-update',
  templateUrl: './unternehmen-update.component.html',
})
export class UnternehmenUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    ort: [null, [Validators.required]],
    branche: [],
    beschreibung: [],
    postleitzahl: [null, [Validators.required]],
    adresse: [null, [Validators.required]],
    land: [null, [Validators.required]],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected unternehmenService: UnternehmenService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ unternehmen }) => {
      this.updateForm(unternehmen);
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('praktikumsverwaltungApp.error', { message: err.message })
        ),
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const unternehmen = this.createFromForm();
    if (unternehmen.id !== undefined) {
      this.subscribeToSaveResponse(this.unternehmenService.update(unternehmen));
    } else {
      this.subscribeToSaveResponse(this.unternehmenService.create(unternehmen));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUnternehmen>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(unternehmen: IUnternehmen): void {
    this.editForm.patchValue({
      id: unternehmen.id,
      name: unternehmen.name,
      ort: unternehmen.ort,
      branche: unternehmen.branche,
      beschreibung: unternehmen.beschreibung,
      postleitzahl: unternehmen.postleitzahl,
      adresse: unternehmen.adresse,
      land: unternehmen.land,
    });
  }

  protected createFromForm(): IUnternehmen {
    return {
      ...new Unternehmen(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      ort: this.editForm.get(['ort'])!.value,
      branche: this.editForm.get(['branche'])!.value,
      beschreibung: this.editForm.get(['beschreibung'])!.value,
      postleitzahl: this.editForm.get(['postleitzahl'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      land: this.editForm.get(['land'])!.value,
    };
  }
}
