import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IUnternehmen, Unternehmen } from '../unternehmen.model';

import { UnternehmenService } from './unternehmen.service';

describe('Service Tests', () => {
  describe('Unternehmen Service', () => {
    let service: UnternehmenService;
    let httpMock: HttpTestingController;
    let elemDefault: IUnternehmen;
    let expectedResult: IUnternehmen | IUnternehmen[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(UnternehmenService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        ort: 'AAAAAAA',
        branche: 'AAAAAAA',
        beschreibung: 'AAAAAAA',
        postleitzahl: 'AAAAAAA',
        adresse: 'AAAAAAA',
        land: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Unternehmen', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Unternehmen()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Unternehmen', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            ort: 'BBBBBB',
            branche: 'BBBBBB',
            beschreibung: 'BBBBBB',
            postleitzahl: 'BBBBBB',
            adresse: 'BBBBBB',
            land: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Unternehmen', () => {
        const patchObject = Object.assign(
          {
            branche: 'BBBBBB',
            beschreibung: 'BBBBBB',
            adresse: 'BBBBBB',
          },
          new Unternehmen()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Unternehmen', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            ort: 'BBBBBB',
            branche: 'BBBBBB',
            beschreibung: 'BBBBBB',
            postleitzahl: 'BBBBBB',
            adresse: 'BBBBBB',
            land: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Unternehmen', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addUnternehmenToCollectionIfMissing', () => {
        it('should add a Unternehmen to an empty array', () => {
          const unternehmen: IUnternehmen = { id: 123 };
          expectedResult = service.addUnternehmenToCollectionIfMissing([], unternehmen);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(unternehmen);
        });

        it('should not add a Unternehmen to an array that contains it', () => {
          const unternehmen: IUnternehmen = { id: 123 };
          const unternehmenCollection: IUnternehmen[] = [
            {
              ...unternehmen,
            },
            { id: 456 },
          ];
          expectedResult = service.addUnternehmenToCollectionIfMissing(unternehmenCollection, unternehmen);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Unternehmen to an array that doesn't contain it", () => {
          const unternehmen: IUnternehmen = { id: 123 };
          const unternehmenCollection: IUnternehmen[] = [{ id: 456 }];
          expectedResult = service.addUnternehmenToCollectionIfMissing(unternehmenCollection, unternehmen);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(unternehmen);
        });

        it('should add only unique Unternehmen to an array', () => {
          const unternehmenArray: IUnternehmen[] = [{ id: 123 }, { id: 456 }, { id: 91903 }];
          const unternehmenCollection: IUnternehmen[] = [{ id: 123 }];
          expectedResult = service.addUnternehmenToCollectionIfMissing(unternehmenCollection, ...unternehmenArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const unternehmen: IUnternehmen = { id: 123 };
          const unternehmen2: IUnternehmen = { id: 456 };
          expectedResult = service.addUnternehmenToCollectionIfMissing([], unternehmen, unternehmen2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(unternehmen);
          expect(expectedResult).toContain(unternehmen2);
        });

        it('should accept null and undefined values', () => {
          const unternehmen: IUnternehmen = { id: 123 };
          expectedResult = service.addUnternehmenToCollectionIfMissing([], null, unternehmen, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(unternehmen);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
