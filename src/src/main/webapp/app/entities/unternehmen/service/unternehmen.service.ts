import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IUnternehmen, getUnternehmenIdentifier } from '../unternehmen.model';

export type EntityResponseType = HttpResponse<IUnternehmen>;
export type EntityArrayResponseType = HttpResponse<IUnternehmen[]>;

@Injectable({ providedIn: 'root' })
export class UnternehmenService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/unternehmen');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(unternehmen: IUnternehmen): Observable<EntityResponseType> {
    return this.http.post<IUnternehmen>(this.resourceUrl, unternehmen, { observe: 'response' });
  }

  update(unternehmen: IUnternehmen): Observable<EntityResponseType> {
    return this.http.put<IUnternehmen>(`${this.resourceUrl}/${getUnternehmenIdentifier(unternehmen) as number}`, unternehmen, {
      observe: 'response',
    });
  }

  partialUpdate(unternehmen: IUnternehmen): Observable<EntityResponseType> {
    return this.http.patch<IUnternehmen>(`${this.resourceUrl}/${getUnternehmenIdentifier(unternehmen) as number}`, unternehmen, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUnternehmen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUnternehmen[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addUnternehmenToCollectionIfMissing(
    unternehmenCollection: IUnternehmen[],
    ...unternehmenToCheck: (IUnternehmen | null | undefined)[]
  ): IUnternehmen[] {
    const unternehmen: IUnternehmen[] = unternehmenToCheck.filter(isPresent);
    if (unternehmen.length > 0) {
      const unternehmenCollectionIdentifiers = unternehmenCollection.map(unternehmenItem => getUnternehmenIdentifier(unternehmenItem)!);
      const unternehmenToAdd = unternehmen.filter(unternehmenItem => {
        const unternehmenIdentifier = getUnternehmenIdentifier(unternehmenItem);
        if (unternehmenIdentifier == null || unternehmenCollectionIdentifiers.includes(unternehmenIdentifier)) {
          return false;
        }
        unternehmenCollectionIdentifiers.push(unternehmenIdentifier);
        return true;
      });
      return [...unternehmenToAdd, ...unternehmenCollection];
    }
    return unternehmenCollection;
  }
}
