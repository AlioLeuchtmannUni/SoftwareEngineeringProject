import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { UnternehmenComponent } from './list/unternehmen.component';
import { UnternehmenDetailComponent } from './detail/unternehmen-detail.component';
import { UnternehmenUpdateComponent } from './update/unternehmen-update.component';
import { UnternehmenDeleteDialogComponent } from './delete/unternehmen-delete-dialog.component';
import { UnternehmenRoutingModule } from './route/unternehmen-routing.module';

@NgModule({
  imports: [SharedModule, UnternehmenRoutingModule],
  declarations: [UnternehmenComponent, UnternehmenDetailComponent, UnternehmenUpdateComponent, UnternehmenDeleteDialogComponent],
  entryComponents: [UnternehmenDeleteDialogComponent],
})
export class UnternehmenModule {}
