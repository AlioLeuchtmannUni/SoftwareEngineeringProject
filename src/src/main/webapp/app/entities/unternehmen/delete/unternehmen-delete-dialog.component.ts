import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IUnternehmen } from '../unternehmen.model';
import { UnternehmenService } from '../service/unternehmen.service';

@Component({
  templateUrl: './unternehmen-delete-dialog.component.html',
})
export class UnternehmenDeleteDialogComponent {
  unternehmen?: IUnternehmen;

  constructor(protected unternehmenService: UnternehmenService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.unternehmenService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
