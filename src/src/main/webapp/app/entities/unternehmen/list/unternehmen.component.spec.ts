import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { UnternehmenService } from '../service/unternehmen.service';

import { UnternehmenComponent } from './unternehmen.component';

describe('Component Tests', () => {
  describe('Unternehmen Management Component', () => {
    let comp: UnternehmenComponent;
    let fixture: ComponentFixture<UnternehmenComponent>;
    let service: UnternehmenService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [UnternehmenComponent],
      })
        .overrideTemplate(UnternehmenComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UnternehmenComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(UnternehmenService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.unternehmen?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
