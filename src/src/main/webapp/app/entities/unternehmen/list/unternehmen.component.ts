import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUnternehmen } from '../unternehmen.model';
import { UnternehmenService } from '../service/unternehmen.service';
import { UnternehmenDeleteDialogComponent } from '../delete/unternehmen-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-unternehmen',
  templateUrl: './unternehmen.component.html',
})
export class UnternehmenComponent implements OnInit {
  unternehmen?: IUnternehmen[];
  isLoading = false;

  constructor(protected unternehmenService: UnternehmenService, protected dataUtils: DataUtils, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.unternehmenService.query().subscribe(
      (res: HttpResponse<IUnternehmen[]>) => {
        this.isLoading = false;
        this.unternehmen = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IUnternehmen): number {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(unternehmen: IUnternehmen): void {
    const modalRef = this.modalService.open(UnternehmenDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.unternehmen = unternehmen;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
