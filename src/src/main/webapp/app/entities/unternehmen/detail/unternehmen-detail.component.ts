import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUnternehmen } from '../unternehmen.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-unternehmen-detail',
  templateUrl: './unternehmen-detail.component.html',
})
export class UnternehmenDetailComponent implements OnInit {
  unternehmen: IUnternehmen | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ unternehmen }) => {
      this.unternehmen = unternehmen;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
