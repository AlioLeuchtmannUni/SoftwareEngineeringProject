import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DataUtils } from 'app/core/util/data-util.service';

import { VertragDetailComponent } from './vertrag-detail.component';

describe('Component Tests', () => {
  describe('Vertrag Management Detail Component', () => {
    let comp: VertragDetailComponent;
    let fixture: ComponentFixture<VertragDetailComponent>;
    let dataUtils: DataUtils;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [VertragDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ vertrag: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(VertragDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VertragDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = TestBed.inject(DataUtils);
    });

    describe('OnInit', () => {
      it('Should load vertrag on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vertrag).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from DataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from DataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeBase64, fakeContentType);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeBase64, fakeContentType);
      });
    });
  });
});
