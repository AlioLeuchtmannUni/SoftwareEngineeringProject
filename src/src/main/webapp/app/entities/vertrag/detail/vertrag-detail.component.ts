import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVertrag } from '../vertrag.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-vertrag-detail',
  templateUrl: './vertrag-detail.component.html',
})
export class VertragDetailComponent implements OnInit {
  vertrag: IVertrag | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vertrag }) => {
      this.vertrag = vertrag;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
