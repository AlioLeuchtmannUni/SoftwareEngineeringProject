import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { VertragComponent } from './list/vertrag.component';
import { VertragDetailComponent } from './detail/vertrag-detail.component';
import { VertragUpdateComponent } from './update/vertrag-update.component';
import { VertragDeleteDialogComponent } from './delete/vertrag-delete-dialog.component';
import { VertragRoutingModule } from './route/vertrag-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  imports: [SharedModule, VertragRoutingModule, MatFormFieldModule, MatAutocompleteModule],
  declarations: [VertragComponent, VertragDetailComponent, VertragUpdateComponent, VertragDeleteDialogComponent],
  entryComponents: [VertragDeleteDialogComponent],
})
export class VertragModule {}
