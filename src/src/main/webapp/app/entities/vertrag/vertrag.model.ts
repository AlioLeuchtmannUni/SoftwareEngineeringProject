import * as dayjs from 'dayjs';
import { IStudent } from 'app/entities/student/student.model';
import { IDozent } from 'app/entities/dozent/dozent.model';
import { IUnternehmen } from 'app/entities/unternehmen/unternehmen.model';
import { BearbeitungsStand } from 'app/entities/enumerations/bearbeitungs-stand.model';

export interface IVertrag {
  id?: number;
  ansprechpartner?: string | null;
  beginn?: dayjs.Dayjs;
  ende?: dayjs.Dayjs;
  bearbeitungsStand?: BearbeitungsStand;
  beschreibung?: string | null;
  praktikant?: IStudent;
  dozent?: IDozent;
  unternehmen?: IUnternehmen;
}

export class Vertrag implements IVertrag {
  constructor(
    public id?: number,
    public ansprechpartner?: string | null,
    public beginn?: dayjs.Dayjs,
    public ende?: dayjs.Dayjs,
    public bearbeitungsStand?: BearbeitungsStand,
    public beschreibung?: string | null,
    public praktikant?: IStudent,
    public dozent?: IDozent,
    public unternehmen?: IUnternehmen
  ) {}
}

export function getVertragIdentifier(vertrag: IVertrag): number | undefined {
  return vertrag.id;
}
