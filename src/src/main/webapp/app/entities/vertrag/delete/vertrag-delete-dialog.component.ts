import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IVertrag } from '../vertrag.model';
import { VertragService } from '../service/vertrag.service';

@Component({
  templateUrl: './vertrag-delete-dialog.component.html',
})
export class VertragDeleteDialogComponent {
  vertrag?: IVertrag;

  constructor(protected vertragService: VertragService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.vertragService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
