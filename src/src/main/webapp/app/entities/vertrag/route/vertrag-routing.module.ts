import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { VertragComponent } from '../list/vertrag.component';
import { VertragDetailComponent } from '../detail/vertrag-detail.component';
import { VertragUpdateComponent } from '../update/vertrag-update.component';
import { VertragRoutingResolveService } from './vertrag-routing-resolve.service';

const vertragRoute: Routes = [
  {
    path: '',
    component: VertragComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VertragDetailComponent,
    resolve: {
      vertrag: VertragRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VertragUpdateComponent,
    resolve: {
      vertrag: VertragRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VertragUpdateComponent,
    resolve: {
      vertrag: VertragRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(vertragRoute)],
  exports: [RouterModule],
})
export class VertragRoutingModule {}
