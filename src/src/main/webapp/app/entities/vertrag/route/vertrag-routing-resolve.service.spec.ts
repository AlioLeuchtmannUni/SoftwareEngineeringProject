jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IVertrag, Vertrag } from '../vertrag.model';
import { VertragService } from '../service/vertrag.service';

import { VertragRoutingResolveService } from './vertrag-routing-resolve.service';

describe('Service Tests', () => {
  describe('Vertrag routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: VertragRoutingResolveService;
    let service: VertragService;
    let resultVertrag: IVertrag | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(VertragRoutingResolveService);
      service = TestBed.inject(VertragService);
      resultVertrag = undefined;
    });

    describe('resolve', () => {
      it('should return IVertrag returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultVertrag = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultVertrag).toEqual({ id: 123 });
      });

      it('should return new IVertrag if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultVertrag = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultVertrag).toEqual(new Vertrag());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultVertrag = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultVertrag).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
