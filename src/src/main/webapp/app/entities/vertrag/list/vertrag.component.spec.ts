import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { VertragService } from '../service/vertrag.service';

import { VertragComponent } from './vertrag.component';

describe('Component Tests', () => {
  describe('Vertrag Management Component', () => {
    let comp: VertragComponent;
    let fixture: ComponentFixture<VertragComponent>;
    let service: VertragService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [VertragComponent],
      })
        .overrideTemplate(VertragComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VertragComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(VertragService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.vertrags?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
