import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVertrag } from '../vertrag.model';
import { VertragService } from '../service/vertrag.service';
import { VertragDeleteDialogComponent } from '../delete/vertrag-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';
import { UserService } from 'app/entities/user/user.service';
import { UserManagementService } from 'app/admin/user-management/service/user-management.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';

@Component({
  selector: 'jhi-vertrag',
  templateUrl: './vertrag.component.html',
})
export class VertragComponent implements OnInit {
  vertrags?: IVertrag[];
  isLoading = false;

  constructor(
    protected vertragService: VertragService,
    protected dataUtils: DataUtils,
    protected modalService: NgbModal,
    protected userService: UserService,
    protected userManagmentService: UserManagementService,
    protected accountService: AccountService
  ) {}

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): void {
    this.isLoading = true;

    let acc: Account | null;

    // Get Current logged in Account Data
    this.accountService.identity(true).subscribe(account => {
      acc = account;
    });

    // Nur passende Verträge
    this.vertragService.query().subscribe(
      (res: HttpResponse<IVertrag[]>) => {
        this.isLoading = false;
        this.vertrags = res.body ?? [];

        // Determine if Restriction needed,  e.g Dozent can only see own Contracts
        const needsRestriction =
          acc?.authorities.find(role => role === 'ROLE_DOZENT' || role === 'ROLE_STUDENT') === undefined ? false : true;

        if (this.vertrags.length > 0 && needsRestriction) {
          for (let i = 0; i < this.vertrags.length; i++) {
            if (!(this.vertrags[i]?.dozent?.email === acc?.email || this.vertrags[i]?.praktikant?.sNummer === acc?.login)) {
              this.vertrags.splice(i, 1);
              i--;
            }
          }
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  filterDozent(): void {
    console.log('1');
  }

  filterStudent(): void {
    console.log('2');
  }

  trackId(index: number, item: IVertrag): number {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(vertrag: IVertrag): void {
    const modalRef = this.modalService.open(VertragDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.vertrag = vertrag;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
