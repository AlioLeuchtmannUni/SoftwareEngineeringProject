import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IVertrag, Vertrag } from '../vertrag.model';
import { VertragService } from '../service/vertrag.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IStudent } from 'app/entities/student/student.model';
import { StudentService } from 'app/entities/student/service/student.service';
import { IDozent } from 'app/entities/dozent/dozent.model';
import { DozentService } from 'app/entities/dozent/service/dozent.service';
import { IUnternehmen } from 'app/entities/unternehmen/unternehmen.model';
import { UnternehmenService } from 'app/entities/unternehmen/service/unternehmen.service';
import * as dayjs from 'dayjs';
import { BearbeitungsStand } from 'app/entities/enumerations/bearbeitungs-stand.model';
import { MailService } from 'app/shared/mail/mail.service';

@Component({
  selector: 'jhi-vertrag-update',
  templateUrl: './vertrag-update.component.html',
  styleUrls: ['./vertrag-update.component.css'],
})
export class VertragUpdateComponent implements OnInit {
  isSaving = false;

  praktikantsCollection: IStudent[] = [];
  dozentsSharedCollection: IDozent[] = [];
  unternehmenSharedCollection: IUnternehmen[] = [];
  bachelor = true;
  wasUpdated = false;
  initialBearbeitungsStand = '';

  praktikanten: IStudent[] = [];
  dozenten: IDozent[] = [];
  unternehmen: IUnternehmen[] = [];
  vertraege: IVertrag[] = [];
  availablePraktikants: IStudent[] = [];

  editForm = this.fb.group({
    id: [],
    ansprechpartner: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    beginn: [new Date(), [Validators.required]],
    ende: [new Date(), [Validators.required]],
    bearbeitungsStand: [null, [Validators.required]],
    beschreibung: [null, [Validators.maxLength, Validators.minLength(3), Validators.minLength(50)]],
    praktikant: [null, [Validators.required]],
    dozent: [null, [Validators.required]],
    unternehmen: [null, [Validators.required]],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected vertragService: VertragService,
    protected studentService: StudentService,
    protected dozentService: DozentService,
    protected unternehmenService: UnternehmenService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder,
    protected mailService: MailService
  ) {}

  getStandValue(bearbeitungsStand: string): number {
    switch (bearbeitungsStand) {
      case 'VertragAngelegt':
        return 1;
      case 'BerichtUndZeugnisVorhanden':
        return 2;
      case 'AnnerkennungErfolgt':
        return 3;
      case 'ErfolgGemeldet':
        return 4;
      default:
        return 0;
    }
  }

  processBearbeitungsStandChange(vertrag: IVertrag): void {
    console.log('nadwd');
    this.mailService.reportStatusChangeToPraktikumsBeauftragtem(vertrag).subscribe(res => console.log(res));
    if (vertrag.bearbeitungsStand === 'ErfolgGemeldet') {
      this.mailService.reportErfolgGemeldetToStudent(vertrag).subscribe(res => console.log(res));
    }
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vertrag }) => {
      if (vertrag.id !== undefined) {
        this.wasUpdated = true;
        this.initialBearbeitungsStand = vertrag.bearbeitungsStand;
      } else {
        this.editForm.get(['bearbeitungsStand'])?.setValue(['VertragAngelegt']);
      }
      this.updateForm(vertrag);
    });

    this.studentService.query().subscribe(data => {
      this.praktikanten = data.body!; // TODO: nullsafe implementation
    });
    this.dozentService.query().subscribe(data => {
      this.dozenten = data.body!; // TODO: nullsafe implementation
    });
    this.unternehmenService.query().subscribe(data => {
      this.unternehmen = data.body!; // TODO: nullsafe implementation
    });
    this.vertragService.query().subscribe(data => {
      this.vertraege = data.body!; // TODO: nullsafe implementation
    });
  }

  getPraktikantOptions(): IStudent[] {
    const fieldValue = this.editForm.get('praktikant')?.value;

    return this.praktikanten.filter(
      currentStudent =>
        currentStudent.sNummer?.startsWith(fieldValue) && // anfang muss stimmen
        fieldValue !== '' && // wenn Formfield leer keine Vorschläge
        fieldValue !== currentStudent.sNummer && // wenn genauer match auch nicht anzeigen
        this.vertraege.findIndex(currentVertrag => currentVertrag.praktikant?.sNummer === currentStudent.sNummer) === -1 // darf noch keine Vertrag haben
    );
  }

  getDozentOptions(): IDozent[] {
    const fieldValue = this.editForm.get('dozent')?.value;

    return this.dozenten.filter(
      currentDozent => currentDozent.email?.startsWith(fieldValue) && fieldValue !== '' && fieldValue !== currentDozent.email
    );
  }

  getUnternehmenOptions(): IUnternehmen[] {
    const fieldValue = this.editForm.get('unternehmen')?.value;

    return this.unternehmen.filter(
      currentUnternehmen => currentUnternehmen.name?.startsWith(fieldValue) && fieldValue !== '' && fieldValue !== currentUnternehmen.name
    );
  }

  // Universell:
  // Liste von Strings wobei die einzelnen Strings Unique sein müssen
  // geht nicht da praktikanten.map(...) nicht möglich in html als Funktionsparameter
  getOptions(list: string[], fieldIdentifier: string): string[] {
    this.praktikanten.map(element => element.sNummer);

    const fieldValue = this.editForm.get(fieldIdentifier)?.value;

    return list.filter(element => element.startsWith(fieldValue) && fieldValue !== '' && fieldValue !== element);
  }

  // TODO: welche gruppen sind Bachelor und welche Diplom herausfinden !
  // TODO: Fehler meldung für nutzer implementieren
  checkPrakLength(): boolean {
    const begin = <Date>this.editForm.get('beginn')?.value;
    const end = <Date>this.editForm.get('ende')?.value;
    const diff = dayjs(end).diff(begin) / 1000 / 60 / 60 / 24;

    console.log(diff);
    // Bachelor
    if (diff < 12 * 7 && this.bachelor) {
      return false;
    }
    // Diplom
    if (diff < 20 * 7 && !this.bachelor) {
      return false;
    }
    return true;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('praktikumsverwaltungApp.error', { message: err.message })
        ),
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vertrag = this.createFromForm();
    if (vertrag.id !== undefined) {
      this.subscribeToSaveResponse(this.vertragService.update(vertrag), vertrag);
    } else {
      this.subscribeToSaveResponse(this.vertragService.create(vertrag), vertrag);
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVertrag>>, vertrag: IVertrag): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(vertrag),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(vertrag: IVertrag): void {
    this.processBearbeitungsStandChange(vertrag);
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(vertrag: IVertrag): void {
    this.editForm.patchValue({
      id: vertrag.id,
      ansprechpartner: vertrag.ansprechpartner,
      beginn: vertrag.beginn,
      ende: vertrag.ende,
      bearbeitungsStand: vertrag.bearbeitungsStand,
      beschreibung: vertrag.beschreibung,
      praktikant: vertrag.praktikant!.sNummer,
      dozent: vertrag.dozent!.email,
      unternehmen: vertrag.unternehmen!.name,
    });
  }

  protected createFromForm(): IVertrag {
    return {
      ...new Vertrag(),
      id: this.editForm.get(['id'])!.value,
      ansprechpartner: this.editForm.get(['ansprechpartner'])!.value,
      beginn: this.editForm.get(['beginn'])!.value,
      ende: this.editForm.get(['ende'])!.value,
      bearbeitungsStand: this.editForm.get('bearbeitungsStand')!.value,
      beschreibung: this.editForm.get(['beschreibung'])!.value,
      praktikant: this.praktikanten.find(praktikant => praktikant.sNummer === this.editForm.get(['praktikant'])!.value),
      dozent: this.dozenten.find(dozent => dozent.email === this.editForm.get(['dozent'])!.value),
      unternehmen: this.unternehmen.find(unternehmen => unternehmen.name === this.editForm.get(['unternehmen'])!.value),
    };
  }
}
