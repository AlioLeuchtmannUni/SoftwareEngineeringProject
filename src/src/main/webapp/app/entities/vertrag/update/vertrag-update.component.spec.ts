jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { VertragService } from '../service/vertrag.service';
import { IVertrag, Vertrag } from '../vertrag.model';
import { IStudent } from 'app/entities/student/student.model';
import { StudentService } from 'app/entities/student/service/student.service';
import { IDozent } from 'app/entities/dozent/dozent.model';
import { DozentService } from 'app/entities/dozent/service/dozent.service';
import { IUnternehmen } from 'app/entities/unternehmen/unternehmen.model';
import { UnternehmenService } from 'app/entities/unternehmen/service/unternehmen.service';

import { VertragUpdateComponent } from './vertrag-update.component';

describe('Component Tests', () => {
  describe('Vertrag Management Update Component', () => {
    let comp: VertragUpdateComponent;
    let fixture: ComponentFixture<VertragUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let vertragService: VertragService;
    let studentService: StudentService;
    let dozentService: DozentService;
    let unternehmenService: UnternehmenService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [VertragUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(VertragUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VertragUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      vertragService = TestBed.inject(VertragService);
      studentService = TestBed.inject(StudentService);
      dozentService = TestBed.inject(DozentService);
      unternehmenService = TestBed.inject(UnternehmenService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call praktikant query and add missing value', () => {
        const vertrag: IVertrag = { id: 456 };
        const praktikant: IStudent = { id: 61512 };
        vertrag.praktikant = praktikant;

        const praktikantCollection: IStudent[] = [{ id: 86635 }];
        spyOn(studentService, 'query').and.returnValue(of(new HttpResponse({ body: praktikantCollection })));
        const expectedCollection: IStudent[] = [praktikant, ...praktikantCollection];
        spyOn(studentService, 'addStudentToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        expect(studentService.query).toHaveBeenCalled();
        expect(studentService.addStudentToCollectionIfMissing).toHaveBeenCalledWith(praktikantCollection, praktikant);
        expect(comp.praktikantsCollection).toEqual(expectedCollection);
      });

      it('Should call Dozent query and add missing value', () => {
        const vertrag: IVertrag = { id: 456 };
        const dozent: IDozent = { id: 43464 };
        vertrag.dozent = dozent;

        const dozentCollection: IDozent[] = [{ id: 36929 }];
        spyOn(dozentService, 'query').and.returnValue(of(new HttpResponse({ body: dozentCollection })));
        const additionalDozents = [dozent];
        const expectedCollection: IDozent[] = [...additionalDozents, ...dozentCollection];
        spyOn(dozentService, 'addDozentToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        expect(dozentService.query).toHaveBeenCalled();
        expect(dozentService.addDozentToCollectionIfMissing).toHaveBeenCalledWith(dozentCollection, ...additionalDozents);
        expect(comp.dozentsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Unternehmen query and add missing value', () => {
        const vertrag: IVertrag = { id: 456 };
        const unternehmen: IUnternehmen = { id: 91946 };
        vertrag.unternehmen = unternehmen;

        const unternehmenCollection: IUnternehmen[] = [{ id: 6645 }];
        spyOn(unternehmenService, 'query').and.returnValue(of(new HttpResponse({ body: unternehmenCollection })));
        const additionalUnternehmen = [unternehmen];
        const expectedCollection: IUnternehmen[] = [...additionalUnternehmen, ...unternehmenCollection];
        spyOn(unternehmenService, 'addUnternehmenToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        expect(unternehmenService.query).toHaveBeenCalled();
        expect(unternehmenService.addUnternehmenToCollectionIfMissing).toHaveBeenCalledWith(
          unternehmenCollection,
          ...additionalUnternehmen
        );
        expect(comp.unternehmenSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const vertrag: IVertrag = { id: 456 };
        const praktikant: IStudent = { id: 13984 };
        vertrag.praktikant = praktikant;
        const dozent: IDozent = { id: 47890 };
        vertrag.dozent = dozent;
        const unternehmen: IUnternehmen = { id: 91903 };
        vertrag.unternehmen = unternehmen;

        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(vertrag));
        expect(comp.praktikantsCollection).toContain(praktikant);
        expect(comp.dozentsSharedCollection).toContain(dozent);
        expect(comp.unternehmenSharedCollection).toContain(unternehmen);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const vertrag = { id: 123 };
        spyOn(vertragService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: vertrag }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(vertragService.update).toHaveBeenCalledWith(vertrag);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const vertrag = new Vertrag();
        spyOn(vertragService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: vertrag }));
        saveSubject.complete();

        // THEN
        expect(vertragService.create).toHaveBeenCalledWith(vertrag);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const vertrag = { id: 123 };
        spyOn(vertragService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ vertrag });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(vertragService.update).toHaveBeenCalledWith(vertrag);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackStudentById', () => {
        it('Should return tracked Student primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackStudentById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackDozentById', () => {
        it('Should return tracked Dozent primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackDozentById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackUnternehmenById', () => {
        it('Should return tracked Unternehmen primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackUnternehmenById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
