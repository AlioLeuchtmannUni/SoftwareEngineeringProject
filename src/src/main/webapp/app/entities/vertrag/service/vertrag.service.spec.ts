import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { BearbeitungsStand } from 'app/entities/enumerations/bearbeitungs-stand.model';
import { IVertrag, Vertrag } from '../vertrag.model';

import { VertragService } from './vertrag.service';

describe('Service Tests', () => {
  describe('Vertrag Service', () => {
    let service: VertragService;
    let httpMock: HttpTestingController;
    let elemDefault: IVertrag;
    let expectedResult: IVertrag | IVertrag[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(VertragService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ansprechpartner: 'AAAAAAA',
        beginn: currentDate,
        ende: currentDate,
        bearbeitungsStand: BearbeitungsStand.VertragAngelegt,
        beschreibung: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            beginn: currentDate.format(DATE_FORMAT),
            ende: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Vertrag', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            beginn: currentDate.format(DATE_FORMAT),
            ende: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            beginn: currentDate,
            ende: currentDate,
          },
          returnedFromService
        );

        service.create(new Vertrag()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Vertrag', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ansprechpartner: 'BBBBBB',
            beginn: currentDate.format(DATE_FORMAT),
            ende: currentDate.format(DATE_FORMAT),
            bearbeitungsStand: 'BBBBBB',
            beschreibung: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            beginn: currentDate,
            ende: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Vertrag', () => {
        const patchObject = Object.assign(
          {
            ansprechpartner: 'BBBBBB',
            beginn: currentDate.format(DATE_FORMAT),
          },
          new Vertrag()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            beginn: currentDate,
            ende: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Vertrag', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ansprechpartner: 'BBBBBB',
            beginn: currentDate.format(DATE_FORMAT),
            ende: currentDate.format(DATE_FORMAT),
            bearbeitungsStand: 'BBBBBB',
            beschreibung: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            beginn: currentDate,
            ende: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Vertrag', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addVertragToCollectionIfMissing', () => {
        it('should add a Vertrag to an empty array', () => {
          const vertrag: IVertrag = { id: 123 };
          expectedResult = service.addVertragToCollectionIfMissing([], vertrag);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(vertrag);
        });

        it('should not add a Vertrag to an array that contains it', () => {
          const vertrag: IVertrag = { id: 123 };
          const vertragCollection: IVertrag[] = [
            {
              ...vertrag,
            },
            { id: 456 },
          ];
          expectedResult = service.addVertragToCollectionIfMissing(vertragCollection, vertrag);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Vertrag to an array that doesn't contain it", () => {
          const vertrag: IVertrag = { id: 123 };
          const vertragCollection: IVertrag[] = [{ id: 456 }];
          expectedResult = service.addVertragToCollectionIfMissing(vertragCollection, vertrag);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(vertrag);
        });

        it('should add only unique Vertrag to an array', () => {
          const vertragArray: IVertrag[] = [{ id: 123 }, { id: 456 }, { id: 82794 }];
          const vertragCollection: IVertrag[] = [{ id: 123 }];
          expectedResult = service.addVertragToCollectionIfMissing(vertragCollection, ...vertragArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const vertrag: IVertrag = { id: 123 };
          const vertrag2: IVertrag = { id: 456 };
          expectedResult = service.addVertragToCollectionIfMissing([], vertrag, vertrag2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(vertrag);
          expect(expectedResult).toContain(vertrag2);
        });

        it('should accept null and undefined values', () => {
          const vertrag: IVertrag = { id: 123 };
          expectedResult = service.addVertragToCollectionIfMissing([], null, vertrag, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(vertrag);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
