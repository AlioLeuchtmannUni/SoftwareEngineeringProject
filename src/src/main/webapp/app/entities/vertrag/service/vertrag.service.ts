import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IVertrag, getVertragIdentifier } from '../vertrag.model';

export type EntityResponseType = HttpResponse<IVertrag>;
export type EntityArrayResponseType = HttpResponse<IVertrag[]>;

@Injectable({ providedIn: 'root' })
export class VertragService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/vertrags');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(vertrag: IVertrag): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vertrag);
    return this.http
      .post<IVertrag>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(vertrag: IVertrag): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vertrag);
    return this.http
      .put<IVertrag>(`${this.resourceUrl}/${getVertragIdentifier(vertrag) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(vertrag: IVertrag): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vertrag);
    return this.http
      .patch<IVertrag>(`${this.resourceUrl}/${getVertragIdentifier(vertrag) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVertrag>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVertrag[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addVertragToCollectionIfMissing(vertragCollection: IVertrag[], ...vertragsToCheck: (IVertrag | null | undefined)[]): IVertrag[] {
    const vertrags: IVertrag[] = vertragsToCheck.filter(isPresent);
    if (vertrags.length > 0) {
      const vertragCollectionIdentifiers = vertragCollection.map(vertragItem => getVertragIdentifier(vertragItem)!);
      const vertragsToAdd = vertrags.filter(vertragItem => {
        const vertragIdentifier = getVertragIdentifier(vertragItem);
        if (vertragIdentifier == null || vertragCollectionIdentifiers.includes(vertragIdentifier)) {
          return false;
        }
        vertragCollectionIdentifiers.push(vertragIdentifier);
        return true;
      });
      return [...vertragsToAdd, ...vertragCollection];
    }
    return vertragCollection;
  }

  protected convertDateFromClient(vertrag: IVertrag): IVertrag {
    return Object.assign({}, vertrag, {
      beginn: vertrag.beginn?.isValid() ? vertrag.beginn.format(DATE_FORMAT) : undefined,
      ende: vertrag.ende?.isValid() ? vertrag.ende.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.beginn = res.body.beginn ? dayjs(res.body.beginn) : undefined;
      res.body.ende = res.body.ende ? dayjs(res.body.ende) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((vertrag: IVertrag) => {
        vertrag.beginn = vertrag.beginn ? dayjs(vertrag.beginn) : undefined;
        vertrag.ende = vertrag.ende ? dayjs(vertrag.ende) : undefined;
      });
    }
    return res;
  }
}
