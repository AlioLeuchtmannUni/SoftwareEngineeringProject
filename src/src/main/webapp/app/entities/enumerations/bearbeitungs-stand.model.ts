export enum BearbeitungsStand {
  VertragAngelegt = 'VertragAngelegt',
  BerichtUndZeugnisVorhanden = 'BerichtUndZeugnisVorhanden',
  AnnerkennungErfolgt = 'AnnerkennungErfolgt',
  ErfolgGemeldet = 'ErfolgGemeldet',
}
