export interface IStudent {
  id?: number;
  matrikelNmr?: string;
  sNummer?: string;
  studienGr?: string;
  nachname?: string;
  vorname?: string;
  beschreibung?: string | null;
}

export class Student implements IStudent {
  constructor(
    public id?: number,
    public matrikelNmr?: string,
    public sNummer?: string,
    public studienGr?: string,
    public nachname?: string,
    public vorname?: string,
    public beschreibung?: string | null
  ) {}
}

export function getStudentIdentifier(student: IStudent): number | undefined {
  return student.id;
}
