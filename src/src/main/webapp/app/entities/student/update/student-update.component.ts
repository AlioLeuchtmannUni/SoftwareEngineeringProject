import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IStudent, Student } from '../student.model';
import { StudentService } from '../service/student.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-student-update',
  templateUrl: './student-update.component.html',
})
export class StudentUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    matrikelNmr: [null, [Validators.required, Validators.pattern('(\\d\\d\\d\\d\\d)')]],
    sNummer: [null, [Validators.required, Validators.pattern('s(\\d\\d\\d\\d\\d)')]],
    studienGr: [null, [Validators.required, Validators.pattern('^(\\d\\d)/((\\d\\d)|(\\d\\d\\d))/((\\d\\d)|(\\d\\d\\d))$')]],
    nachname: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    vorname: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    beschreibung: [Validators.maxLength(200)],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected studentService: StudentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ student }) => {
      this.updateForm(student);
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('praktikumsverwaltungApp.error', { message: err.message })
        ),
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const student = this.createFromForm();
    if (student.id !== undefined) {
      this.subscribeToSaveResponse(this.studentService.update(student));
    } else {
      this.subscribeToSaveResponse(this.studentService.create(student));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudent>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(student: IStudent): void {
    this.editForm.patchValue({
      id: student.id,
      matrikelNmr: student.matrikelNmr,
      sNummer: student.sNummer,
      studienGr: student.studienGr,
      nachname: student.nachname,
      vorname: student.vorname,
      beschreibung: student.beschreibung,
    });
  }

  protected createFromForm(): IStudent {
    return {
      ...new Student(),
      id: this.editForm.get(['id'])!.value,
      matrikelNmr: this.editForm.get(['matrikelNmr'])!.value,
      sNummer: this.editForm.get(['sNummer'])!.value,
      studienGr: this.editForm.get(['studienGr'])!.value,
      nachname: this.editForm.get(['nachname'])!.value,
      vorname: this.editForm.get(['vorname'])!.value,
      beschreibung: this.editForm.get(['beschreibung'])!.value,
    };
  }
}
