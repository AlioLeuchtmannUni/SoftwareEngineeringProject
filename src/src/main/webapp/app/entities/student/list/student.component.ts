import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStudent } from '../student.model';
import { StudentService } from '../service/student.service';
import { StudentDeleteDialogComponent } from '../delete/student-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';
import {AccountService} from "app/core/auth/account.service";
import {Account} from "app/core/auth/account.model";
import {IVertrag} from "app/entities/vertrag/vertrag.model";
import {VertragService} from "app/entities/vertrag/service/vertrag.service";

@Component({
  selector: 'jhi-student',
  templateUrl: './student.component.html',
})
export class StudentComponent implements OnInit {
  students?: IStudent[];
  vertraege?: IVertrag[];
  isLoading = false;

  constructor(
    protected studentService: StudentService,
    protected dataUtils: DataUtils,
    protected modalService: NgbModal,
    protected accountService: AccountService,
    protected vertragService: VertragService) {}

  loadAll(): void {
    this.isLoading = true;


    // Get Current logged in Account Data
    let acc:Account|null;
    this.accountService.identity(true).subscribe( (account)=>{
      acc=account;

      // Get Verträge und filtere nach denen des dozenten
        this.vertragService.query().subscribe(
          (res: HttpResponse<IVertrag[]>) => {
            this.vertraege = res.body ?? [];

            // entferene alle in denen der Dozent nicht vorkommt
            if(this.vertraege.length > 0){
              for(let i=0; i<this.vertraege.length;i++){
                if(this.vertraege[i]?.dozent?.email !== acc?.email) {
                  this.vertraege.splice(i,1);
                  i--;
                }
              }
            }

            // finde passende Studenten
            this.studentService.query().subscribe(
              (res2: HttpResponse<IStudent[]>) => {
                this.isLoading = false;
                this.students = res2.body ?? [];

                // Determine if Restriction needed,  e.g Dozent can only see own Contracts
                const needsRestriction = (acc?.authorities.find( role => (role==='ROLE_DOZENT' || role==='ROLE_STUDENT')))===undefined?false:true;

                if(this.students.length>0 && needsRestriction){
                  for(let i=0; i<this.students.length;i++){

                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore // unschöne Lösung
                    const profHasStudent:boolean = this.vertraege?.find(vertrag => vertrag.praktikant?.sNummer === this.students[i].sNummer) !== undefined;
                    const isLoggedInStudent:boolean = this.students[i]?.sNummer === acc?.login;
                    if(!(isLoggedInStudent || profHasStudent) ) {
                      this.students.splice(i,1);
                      i--;
                    }
                  }
                }
              },
              () => {
                this.isLoading = false;
              }
            );

          }
        )
    });

  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IStudent): number {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(student: IStudent): void {
    const modalRef = this.modalService.open(StudentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.student = student;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
