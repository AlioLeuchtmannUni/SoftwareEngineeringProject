package praktikumsverwaltung.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import praktikumsverwaltung.IntegrationTest;
import praktikumsverwaltung.domain.Dozent;
import praktikumsverwaltung.domain.Student;
import praktikumsverwaltung.domain.Unternehmen;
import praktikumsverwaltung.domain.Vertrag;
import praktikumsverwaltung.domain.enumeration.BearbeitungsStand;
import praktikumsverwaltung.repository.VertragRepository;

/**
 * Integration tests for the {@link VertragResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class VertragResourceIT {

    private static final String DEFAULT_ANSPRECHPARTNER = "AAAAAAAAAA";
    private static final String UPDATED_ANSPRECHPARTNER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BEGINN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BEGINN = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ENDE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ENDE = LocalDate.now(ZoneId.systemDefault());

    private static final BearbeitungsStand DEFAULT_BEARBEITUNGS_STAND = BearbeitungsStand.VertragAngelegt;
    private static final BearbeitungsStand UPDATED_BEARBEITUNGS_STAND = BearbeitungsStand.BerichtUndZeugnisVorhanden;

    private static final String DEFAULT_BESCHREIBUNG = "AAAAAAAAAA";
    private static final String UPDATED_BESCHREIBUNG = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/vertrags";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VertragRepository vertragRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVertragMockMvc;

    private Vertrag vertrag;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vertrag createEntity(EntityManager em) {
        Vertrag vertrag = new Vertrag()
            .ansprechpartner(DEFAULT_ANSPRECHPARTNER)
            .beginn(DEFAULT_BEGINN)
            .ende(DEFAULT_ENDE)
            .bearbeitungsStand(DEFAULT_BEARBEITUNGS_STAND)
            .beschreibung(DEFAULT_BESCHREIBUNG);
        // Add required entity
        Student student;
        if (TestUtil.findAll(em, Student.class).isEmpty()) {
            student = StudentResourceIT.createEntity(em);
            em.persist(student);
            em.flush();
        } else {
            student = TestUtil.findAll(em, Student.class).get(0);
        }
        vertrag.setPraktikant(student);
        // Add required entity
        Dozent dozent;
        if (TestUtil.findAll(em, Dozent.class).isEmpty()) {
            dozent = DozentResourceIT.createEntity(em);
            em.persist(dozent);
            em.flush();
        } else {
            dozent = TestUtil.findAll(em, Dozent.class).get(0);
        }
        vertrag.setDozent(dozent);
        // Add required entity
        Unternehmen unternehmen;
        if (TestUtil.findAll(em, Unternehmen.class).isEmpty()) {
            unternehmen = UnternehmenResourceIT.createEntity(em);
            em.persist(unternehmen);
            em.flush();
        } else {
            unternehmen = TestUtil.findAll(em, Unternehmen.class).get(0);
        }
        vertrag.setUnternehmen(unternehmen);
        return vertrag;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vertrag createUpdatedEntity(EntityManager em) {
        Vertrag vertrag = new Vertrag()
            .ansprechpartner(UPDATED_ANSPRECHPARTNER)
            .beginn(UPDATED_BEGINN)
            .ende(UPDATED_ENDE)
            .bearbeitungsStand(UPDATED_BEARBEITUNGS_STAND)
            .beschreibung(UPDATED_BESCHREIBUNG);
        // Add required entity
        Student student;
        if (TestUtil.findAll(em, Student.class).isEmpty()) {
            student = StudentResourceIT.createUpdatedEntity(em);
            em.persist(student);
            em.flush();
        } else {
            student = TestUtil.findAll(em, Student.class).get(0);
        }
        vertrag.setPraktikant(student);
        // Add required entity
        Dozent dozent;
        if (TestUtil.findAll(em, Dozent.class).isEmpty()) {
            dozent = DozentResourceIT.createUpdatedEntity(em);
            em.persist(dozent);
            em.flush();
        } else {
            dozent = TestUtil.findAll(em, Dozent.class).get(0);
        }
        vertrag.setDozent(dozent);
        // Add required entity
        Unternehmen unternehmen;
        if (TestUtil.findAll(em, Unternehmen.class).isEmpty()) {
            unternehmen = UnternehmenResourceIT.createUpdatedEntity(em);
            em.persist(unternehmen);
            em.flush();
        } else {
            unternehmen = TestUtil.findAll(em, Unternehmen.class).get(0);
        }
        vertrag.setUnternehmen(unternehmen);
        return vertrag;
    }

    @BeforeEach
    public void initTest() {
        vertrag = createEntity(em);
    }

    @Test
    @Transactional
    void createVertrag() throws Exception {
        int databaseSizeBeforeCreate = vertragRepository.findAll().size();
        // Create the Vertrag
        restVertragMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isCreated());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeCreate + 1);
        Vertrag testVertrag = vertragList.get(vertragList.size() - 1);
        assertThat(testVertrag.getAnsprechpartner()).isEqualTo(DEFAULT_ANSPRECHPARTNER);
        assertThat(testVertrag.getBeginn()).isEqualTo(DEFAULT_BEGINN);
        assertThat(testVertrag.getEnde()).isEqualTo(DEFAULT_ENDE);
        assertThat(testVertrag.getBearbeitungsStand()).isEqualTo(DEFAULT_BEARBEITUNGS_STAND);
        assertThat(testVertrag.getBeschreibung()).isEqualTo(DEFAULT_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void createVertragWithExistingId() throws Exception {
        // Create the Vertrag with an existing ID
        vertrag.setId(1L);

        int databaseSizeBeforeCreate = vertragRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVertragMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isBadRequest());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBeginnIsRequired() throws Exception {
        int databaseSizeBeforeTest = vertragRepository.findAll().size();
        // set the field null
        vertrag.setBeginn(null);

        // Create the Vertrag, which fails.

        restVertragMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isBadRequest());

        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEndeIsRequired() throws Exception {
        int databaseSizeBeforeTest = vertragRepository.findAll().size();
        // set the field null
        vertrag.setEnde(null);

        // Create the Vertrag, which fails.

        restVertragMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isBadRequest());

        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBearbeitungsStandIsRequired() throws Exception {
        int databaseSizeBeforeTest = vertragRepository.findAll().size();
        // set the field null
        vertrag.setBearbeitungsStand(null);

        // Create the Vertrag, which fails.

        restVertragMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isBadRequest());

        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllVertrags() throws Exception {
        // Initialize the database
        vertragRepository.saveAndFlush(vertrag);

        // Get all the vertragList
        restVertragMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vertrag.getId().intValue())))
            .andExpect(jsonPath("$.[*].ansprechpartner").value(hasItem(DEFAULT_ANSPRECHPARTNER)))
            .andExpect(jsonPath("$.[*].beginn").value(hasItem(DEFAULT_BEGINN.toString())))
            .andExpect(jsonPath("$.[*].ende").value(hasItem(DEFAULT_ENDE.toString())))
            .andExpect(jsonPath("$.[*].bearbeitungsStand").value(hasItem(DEFAULT_BEARBEITUNGS_STAND.toString())))
            .andExpect(jsonPath("$.[*].beschreibung").value(hasItem(DEFAULT_BESCHREIBUNG.toString())));
    }

    @Test
    @Transactional
    void getVertrag() throws Exception {
        // Initialize the database
        vertragRepository.saveAndFlush(vertrag);

        // Get the vertrag
        restVertragMockMvc
            .perform(get(ENTITY_API_URL_ID, vertrag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vertrag.getId().intValue()))
            .andExpect(jsonPath("$.ansprechpartner").value(DEFAULT_ANSPRECHPARTNER))
            .andExpect(jsonPath("$.beginn").value(DEFAULT_BEGINN.toString()))
            .andExpect(jsonPath("$.ende").value(DEFAULT_ENDE.toString()))
            .andExpect(jsonPath("$.bearbeitungsStand").value(DEFAULT_BEARBEITUNGS_STAND.toString()))
            .andExpect(jsonPath("$.beschreibung").value(DEFAULT_BESCHREIBUNG.toString()));
    }

    @Test
    @Transactional
    void getNonExistingVertrag() throws Exception {
        // Get the vertrag
        restVertragMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewVertrag() throws Exception {
        // Initialize the database
        vertragRepository.saveAndFlush(vertrag);

        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();

        // Update the vertrag
        Vertrag updatedVertrag = vertragRepository.findById(vertrag.getId()).get();
        // Disconnect from session so that the updates on updatedVertrag are not directly saved in db
        em.detach(updatedVertrag);
        updatedVertrag
            .ansprechpartner(UPDATED_ANSPRECHPARTNER)
            .beginn(UPDATED_BEGINN)
            .ende(UPDATED_ENDE)
            .bearbeitungsStand(UPDATED_BEARBEITUNGS_STAND)
            .beschreibung(UPDATED_BESCHREIBUNG);

        restVertragMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedVertrag.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedVertrag))
            )
            .andExpect(status().isOk());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
        Vertrag testVertrag = vertragList.get(vertragList.size() - 1);
        assertThat(testVertrag.getAnsprechpartner()).isEqualTo(UPDATED_ANSPRECHPARTNER);
        assertThat(testVertrag.getBeginn()).isEqualTo(UPDATED_BEGINN);
        assertThat(testVertrag.getEnde()).isEqualTo(UPDATED_ENDE);
        assertThat(testVertrag.getBearbeitungsStand()).isEqualTo(UPDATED_BEARBEITUNGS_STAND);
        assertThat(testVertrag.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void putNonExistingVertrag() throws Exception {
        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();
        vertrag.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVertragMockMvc
            .perform(
                put(ENTITY_API_URL_ID, vertrag.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vertrag))
            )
            .andExpect(status().isBadRequest());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchVertrag() throws Exception {
        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();
        vertrag.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVertragMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vertrag))
            )
            .andExpect(status().isBadRequest());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamVertrag() throws Exception {
        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();
        vertrag.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVertragMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVertragWithPatch() throws Exception {
        // Initialize the database
        vertragRepository.saveAndFlush(vertrag);

        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();

        // Update the vertrag using partial update
        Vertrag partialUpdatedVertrag = new Vertrag();
        partialUpdatedVertrag.setId(vertrag.getId());

        partialUpdatedVertrag.ansprechpartner(UPDATED_ANSPRECHPARTNER).beginn(UPDATED_BEGINN);

        restVertragMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVertrag.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVertrag))
            )
            .andExpect(status().isOk());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
        Vertrag testVertrag = vertragList.get(vertragList.size() - 1);
        assertThat(testVertrag.getAnsprechpartner()).isEqualTo(UPDATED_ANSPRECHPARTNER);
        assertThat(testVertrag.getBeginn()).isEqualTo(UPDATED_BEGINN);
        assertThat(testVertrag.getEnde()).isEqualTo(DEFAULT_ENDE);
        assertThat(testVertrag.getBearbeitungsStand()).isEqualTo(DEFAULT_BEARBEITUNGS_STAND);
        assertThat(testVertrag.getBeschreibung()).isEqualTo(DEFAULT_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void fullUpdateVertragWithPatch() throws Exception {
        // Initialize the database
        vertragRepository.saveAndFlush(vertrag);

        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();

        // Update the vertrag using partial update
        Vertrag partialUpdatedVertrag = new Vertrag();
        partialUpdatedVertrag.setId(vertrag.getId());

        partialUpdatedVertrag
            .ansprechpartner(UPDATED_ANSPRECHPARTNER)
            .beginn(UPDATED_BEGINN)
            .ende(UPDATED_ENDE)
            .bearbeitungsStand(UPDATED_BEARBEITUNGS_STAND)
            .beschreibung(UPDATED_BESCHREIBUNG);

        restVertragMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVertrag.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVertrag))
            )
            .andExpect(status().isOk());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
        Vertrag testVertrag = vertragList.get(vertragList.size() - 1);
        assertThat(testVertrag.getAnsprechpartner()).isEqualTo(UPDATED_ANSPRECHPARTNER);
        assertThat(testVertrag.getBeginn()).isEqualTo(UPDATED_BEGINN);
        assertThat(testVertrag.getEnde()).isEqualTo(UPDATED_ENDE);
        assertThat(testVertrag.getBearbeitungsStand()).isEqualTo(UPDATED_BEARBEITUNGS_STAND);
        assertThat(testVertrag.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void patchNonExistingVertrag() throws Exception {
        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();
        vertrag.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVertragMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, vertrag.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vertrag))
            )
            .andExpect(status().isBadRequest());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchVertrag() throws Exception {
        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();
        vertrag.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVertragMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vertrag))
            )
            .andExpect(status().isBadRequest());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamVertrag() throws Exception {
        int databaseSizeBeforeUpdate = vertragRepository.findAll().size();
        vertrag.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVertragMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(vertrag)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Vertrag in the database
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteVertrag() throws Exception {
        // Initialize the database
        vertragRepository.saveAndFlush(vertrag);

        int databaseSizeBeforeDelete = vertragRepository.findAll().size();

        // Delete the vertrag
        restVertragMockMvc
            .perform(delete(ENTITY_API_URL_ID, vertrag.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Vertrag> vertragList = vertragRepository.findAll();
        assertThat(vertragList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
