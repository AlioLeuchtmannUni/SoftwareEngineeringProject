package praktikumsverwaltung.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import praktikumsverwaltung.IntegrationTest;
import praktikumsverwaltung.domain.Dozent;
import praktikumsverwaltung.repository.DozentRepository;

/**
 * Integration tests for the {@link DozentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DozentResourceIT {

    private static final String DEFAULT_NACHNAME = "AAAAAAAAAA";
    private static final String UPDATED_NACHNAME = "BBBBBBBBBB";

    private static final String DEFAULT_VORNAME = "AAAAAAAAAA";
    private static final String UPDATED_VORNAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_BESCHREIBUNG = "AAAAAAAAAA";
    private static final String UPDATED_BESCHREIBUNG = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/dozents";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DozentRepository dozentRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDozentMockMvc;

    private Dozent dozent;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dozent createEntity(EntityManager em) {
        Dozent dozent = new Dozent()
            .nachname(DEFAULT_NACHNAME)
            .vorname(DEFAULT_VORNAME)
            .email(DEFAULT_EMAIL)
            .beschreibung(DEFAULT_BESCHREIBUNG);
        return dozent;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dozent createUpdatedEntity(EntityManager em) {
        Dozent dozent = new Dozent()
            .nachname(UPDATED_NACHNAME)
            .vorname(UPDATED_VORNAME)
            .email(UPDATED_EMAIL)
            .beschreibung(UPDATED_BESCHREIBUNG);
        return dozent;
    }

    @BeforeEach
    public void initTest() {
        dozent = createEntity(em);
    }

    @Test
    @Transactional
    void createDozent() throws Exception {
        int databaseSizeBeforeCreate = dozentRepository.findAll().size();
        // Create the Dozent
        restDozentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isCreated());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeCreate + 1);
        Dozent testDozent = dozentList.get(dozentList.size() - 1);
        assertThat(testDozent.getNachname()).isEqualTo(DEFAULT_NACHNAME);
        assertThat(testDozent.getVorname()).isEqualTo(DEFAULT_VORNAME);
        assertThat(testDozent.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testDozent.getBeschreibung()).isEqualTo(DEFAULT_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void createDozentWithExistingId() throws Exception {
        // Create the Dozent with an existing ID
        dozent.setId(1L);

        int databaseSizeBeforeCreate = dozentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDozentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isBadRequest());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNachnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dozentRepository.findAll().size();
        // set the field null
        dozent.setNachname(null);

        // Create the Dozent, which fails.

        restDozentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isBadRequest());

        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkVornameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dozentRepository.findAll().size();
        // set the field null
        dozent.setVorname(null);

        // Create the Dozent, which fails.

        restDozentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isBadRequest());

        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = dozentRepository.findAll().size();
        // set the field null
        dozent.setEmail(null);

        // Create the Dozent, which fails.

        restDozentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isBadRequest());

        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDozents() throws Exception {
        // Initialize the database
        dozentRepository.saveAndFlush(dozent);

        // Get all the dozentList
        restDozentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dozent.getId().intValue())))
            .andExpect(jsonPath("$.[*].nachname").value(hasItem(DEFAULT_NACHNAME)))
            .andExpect(jsonPath("$.[*].vorname").value(hasItem(DEFAULT_VORNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].beschreibung").value(hasItem(DEFAULT_BESCHREIBUNG.toString())));
    }

    @Test
    @Transactional
    void getDozent() throws Exception {
        // Initialize the database
        dozentRepository.saveAndFlush(dozent);

        // Get the dozent
        restDozentMockMvc
            .perform(get(ENTITY_API_URL_ID, dozent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dozent.getId().intValue()))
            .andExpect(jsonPath("$.nachname").value(DEFAULT_NACHNAME))
            .andExpect(jsonPath("$.vorname").value(DEFAULT_VORNAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.beschreibung").value(DEFAULT_BESCHREIBUNG.toString()));
    }

    @Test
    @Transactional
    void getNonExistingDozent() throws Exception {
        // Get the dozent
        restDozentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDozent() throws Exception {
        // Initialize the database
        dozentRepository.saveAndFlush(dozent);

        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();

        // Update the dozent
        Dozent updatedDozent = dozentRepository.findById(dozent.getId()).get();
        // Disconnect from session so that the updates on updatedDozent are not directly saved in db
        em.detach(updatedDozent);
        updatedDozent.nachname(UPDATED_NACHNAME).vorname(UPDATED_VORNAME).email(UPDATED_EMAIL).beschreibung(UPDATED_BESCHREIBUNG);

        restDozentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDozent.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDozent))
            )
            .andExpect(status().isOk());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
        Dozent testDozent = dozentList.get(dozentList.size() - 1);
        assertThat(testDozent.getNachname()).isEqualTo(UPDATED_NACHNAME);
        assertThat(testDozent.getVorname()).isEqualTo(UPDATED_VORNAME);
        assertThat(testDozent.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testDozent.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void putNonExistingDozent() throws Exception {
        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();
        dozent.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDozentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dozent.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dozent))
            )
            .andExpect(status().isBadRequest());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDozent() throws Exception {
        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();
        dozent.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDozentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dozent))
            )
            .andExpect(status().isBadRequest());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDozent() throws Exception {
        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();
        dozent.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDozentMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDozentWithPatch() throws Exception {
        // Initialize the database
        dozentRepository.saveAndFlush(dozent);

        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();

        // Update the dozent using partial update
        Dozent partialUpdatedDozent = new Dozent();
        partialUpdatedDozent.setId(dozent.getId());

        partialUpdatedDozent.beschreibung(UPDATED_BESCHREIBUNG);

        restDozentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDozent.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDozent))
            )
            .andExpect(status().isOk());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
        Dozent testDozent = dozentList.get(dozentList.size() - 1);
        assertThat(testDozent.getNachname()).isEqualTo(DEFAULT_NACHNAME);
        assertThat(testDozent.getVorname()).isEqualTo(DEFAULT_VORNAME);
        assertThat(testDozent.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testDozent.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void fullUpdateDozentWithPatch() throws Exception {
        // Initialize the database
        dozentRepository.saveAndFlush(dozent);

        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();

        // Update the dozent using partial update
        Dozent partialUpdatedDozent = new Dozent();
        partialUpdatedDozent.setId(dozent.getId());

        partialUpdatedDozent.nachname(UPDATED_NACHNAME).vorname(UPDATED_VORNAME).email(UPDATED_EMAIL).beschreibung(UPDATED_BESCHREIBUNG);

        restDozentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDozent.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDozent))
            )
            .andExpect(status().isOk());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
        Dozent testDozent = dozentList.get(dozentList.size() - 1);
        assertThat(testDozent.getNachname()).isEqualTo(UPDATED_NACHNAME);
        assertThat(testDozent.getVorname()).isEqualTo(UPDATED_VORNAME);
        assertThat(testDozent.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testDozent.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
    }

    @Test
    @Transactional
    void patchNonExistingDozent() throws Exception {
        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();
        dozent.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDozentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dozent.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dozent))
            )
            .andExpect(status().isBadRequest());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDozent() throws Exception {
        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();
        dozent.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDozentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dozent))
            )
            .andExpect(status().isBadRequest());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDozent() throws Exception {
        int databaseSizeBeforeUpdate = dozentRepository.findAll().size();
        dozent.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDozentMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dozent)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Dozent in the database
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDozent() throws Exception {
        // Initialize the database
        dozentRepository.saveAndFlush(dozent);

        int databaseSizeBeforeDelete = dozentRepository.findAll().size();

        // Delete the dozent
        restDozentMockMvc
            .perform(delete(ENTITY_API_URL_ID, dozent.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Dozent> dozentList = dozentRepository.findAll();
        assertThat(dozentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
