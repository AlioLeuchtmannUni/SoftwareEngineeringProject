package praktikumsverwaltung.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import praktikumsverwaltung.IntegrationTest;
import praktikumsverwaltung.domain.Unternehmen;
import praktikumsverwaltung.repository.UnternehmenRepository;

/**
 * Integration tests for the {@link UnternehmenResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UnternehmenResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ORT = "AAAAAAAAAA";
    private static final String UPDATED_ORT = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCHE = "AAAAAAAAAA";
    private static final String UPDATED_BRANCHE = "BBBBBBBBBB";

    private static final String DEFAULT_BESCHREIBUNG = "AAAAAAAAAA";
    private static final String UPDATED_BESCHREIBUNG = "BBBBBBBBBB";

    private static final String DEFAULT_POSTLEITZAHL = "AAAAAAAAAA";
    private static final String UPDATED_POSTLEITZAHL = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_LAND = "AAAAAAAAAA";
    private static final String UPDATED_LAND = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/unternehmen";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UnternehmenRepository unternehmenRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUnternehmenMockMvc;

    private Unternehmen unternehmen;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Unternehmen createEntity(EntityManager em) {
        Unternehmen unternehmen = new Unternehmen()
            .name(DEFAULT_NAME)
            .ort(DEFAULT_ORT)
            .branche(DEFAULT_BRANCHE)
            .beschreibung(DEFAULT_BESCHREIBUNG)
            .postleitzahl(DEFAULT_POSTLEITZAHL)
            .adresse(DEFAULT_ADRESSE)
            .land(DEFAULT_LAND);
        return unternehmen;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Unternehmen createUpdatedEntity(EntityManager em) {
        Unternehmen unternehmen = new Unternehmen()
            .name(UPDATED_NAME)
            .ort(UPDATED_ORT)
            .branche(UPDATED_BRANCHE)
            .beschreibung(UPDATED_BESCHREIBUNG)
            .postleitzahl(UPDATED_POSTLEITZAHL)
            .adresse(UPDATED_ADRESSE)
            .land(UPDATED_LAND);
        return unternehmen;
    }

    @BeforeEach
    public void initTest() {
        unternehmen = createEntity(em);
    }

    @Test
    @Transactional
    void createUnternehmen() throws Exception {
        int databaseSizeBeforeCreate = unternehmenRepository.findAll().size();
        // Create the Unternehmen
        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isCreated());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeCreate + 1);
        Unternehmen testUnternehmen = unternehmenList.get(unternehmenList.size() - 1);
        assertThat(testUnternehmen.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUnternehmen.getOrt()).isEqualTo(DEFAULT_ORT);
        assertThat(testUnternehmen.getBranche()).isEqualTo(DEFAULT_BRANCHE);
        assertThat(testUnternehmen.getBeschreibung()).isEqualTo(DEFAULT_BESCHREIBUNG);
        assertThat(testUnternehmen.getPostleitzahl()).isEqualTo(DEFAULT_POSTLEITZAHL);
        assertThat(testUnternehmen.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testUnternehmen.getLand()).isEqualTo(DEFAULT_LAND);
    }

    @Test
    @Transactional
    void createUnternehmenWithExistingId() throws Exception {
        // Create the Unternehmen with an existing ID
        unternehmen.setId(1L);

        int databaseSizeBeforeCreate = unternehmenRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isBadRequest());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = unternehmenRepository.findAll().size();
        // set the field null
        unternehmen.setName(null);

        // Create the Unternehmen, which fails.

        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isBadRequest());

        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkOrtIsRequired() throws Exception {
        int databaseSizeBeforeTest = unternehmenRepository.findAll().size();
        // set the field null
        unternehmen.setOrt(null);

        // Create the Unternehmen, which fails.

        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isBadRequest());

        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPostleitzahlIsRequired() throws Exception {
        int databaseSizeBeforeTest = unternehmenRepository.findAll().size();
        // set the field null
        unternehmen.setPostleitzahl(null);

        // Create the Unternehmen, which fails.

        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isBadRequest());

        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAdresseIsRequired() throws Exception {
        int databaseSizeBeforeTest = unternehmenRepository.findAll().size();
        // set the field null
        unternehmen.setAdresse(null);

        // Create the Unternehmen, which fails.

        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isBadRequest());

        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLandIsRequired() throws Exception {
        int databaseSizeBeforeTest = unternehmenRepository.findAll().size();
        // set the field null
        unternehmen.setLand(null);

        // Create the Unternehmen, which fails.

        restUnternehmenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isBadRequest());

        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllUnternehmen() throws Exception {
        // Initialize the database
        unternehmenRepository.saveAndFlush(unternehmen);

        // Get all the unternehmenList
        restUnternehmenMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unternehmen.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].ort").value(hasItem(DEFAULT_ORT)))
            .andExpect(jsonPath("$.[*].branche").value(hasItem(DEFAULT_BRANCHE)))
            .andExpect(jsonPath("$.[*].beschreibung").value(hasItem(DEFAULT_BESCHREIBUNG.toString())))
            .andExpect(jsonPath("$.[*].postleitzahl").value(hasItem(DEFAULT_POSTLEITZAHL)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].land").value(hasItem(DEFAULT_LAND)));
    }

    @Test
    @Transactional
    void getUnternehmen() throws Exception {
        // Initialize the database
        unternehmenRepository.saveAndFlush(unternehmen);

        // Get the unternehmen
        restUnternehmenMockMvc
            .perform(get(ENTITY_API_URL_ID, unternehmen.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(unternehmen.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.ort").value(DEFAULT_ORT))
            .andExpect(jsonPath("$.branche").value(DEFAULT_BRANCHE))
            .andExpect(jsonPath("$.beschreibung").value(DEFAULT_BESCHREIBUNG.toString()))
            .andExpect(jsonPath("$.postleitzahl").value(DEFAULT_POSTLEITZAHL))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.land").value(DEFAULT_LAND));
    }

    @Test
    @Transactional
    void getNonExistingUnternehmen() throws Exception {
        // Get the unternehmen
        restUnternehmenMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewUnternehmen() throws Exception {
        // Initialize the database
        unternehmenRepository.saveAndFlush(unternehmen);

        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();

        // Update the unternehmen
        Unternehmen updatedUnternehmen = unternehmenRepository.findById(unternehmen.getId()).get();
        // Disconnect from session so that the updates on updatedUnternehmen are not directly saved in db
        em.detach(updatedUnternehmen);
        updatedUnternehmen
            .name(UPDATED_NAME)
            .ort(UPDATED_ORT)
            .branche(UPDATED_BRANCHE)
            .beschreibung(UPDATED_BESCHREIBUNG)
            .postleitzahl(UPDATED_POSTLEITZAHL)
            .adresse(UPDATED_ADRESSE)
            .land(UPDATED_LAND);

        restUnternehmenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedUnternehmen.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedUnternehmen))
            )
            .andExpect(status().isOk());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
        Unternehmen testUnternehmen = unternehmenList.get(unternehmenList.size() - 1);
        assertThat(testUnternehmen.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUnternehmen.getOrt()).isEqualTo(UPDATED_ORT);
        assertThat(testUnternehmen.getBranche()).isEqualTo(UPDATED_BRANCHE);
        assertThat(testUnternehmen.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
        assertThat(testUnternehmen.getPostleitzahl()).isEqualTo(UPDATED_POSTLEITZAHL);
        assertThat(testUnternehmen.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testUnternehmen.getLand()).isEqualTo(UPDATED_LAND);
    }

    @Test
    @Transactional
    void putNonExistingUnternehmen() throws Exception {
        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();
        unternehmen.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUnternehmenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, unternehmen.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(unternehmen))
            )
            .andExpect(status().isBadRequest());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUnternehmen() throws Exception {
        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();
        unternehmen.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnternehmenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(unternehmen))
            )
            .andExpect(status().isBadRequest());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUnternehmen() throws Exception {
        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();
        unternehmen.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnternehmenMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unternehmen)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUnternehmenWithPatch() throws Exception {
        // Initialize the database
        unternehmenRepository.saveAndFlush(unternehmen);

        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();

        // Update the unternehmen using partial update
        Unternehmen partialUpdatedUnternehmen = new Unternehmen();
        partialUpdatedUnternehmen.setId(unternehmen.getId());

        partialUpdatedUnternehmen.branche(UPDATED_BRANCHE).beschreibung(UPDATED_BESCHREIBUNG).adresse(UPDATED_ADRESSE);

        restUnternehmenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUnternehmen.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUnternehmen))
            )
            .andExpect(status().isOk());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
        Unternehmen testUnternehmen = unternehmenList.get(unternehmenList.size() - 1);
        assertThat(testUnternehmen.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUnternehmen.getOrt()).isEqualTo(DEFAULT_ORT);
        assertThat(testUnternehmen.getBranche()).isEqualTo(UPDATED_BRANCHE);
        assertThat(testUnternehmen.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
        assertThat(testUnternehmen.getPostleitzahl()).isEqualTo(DEFAULT_POSTLEITZAHL);
        assertThat(testUnternehmen.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testUnternehmen.getLand()).isEqualTo(DEFAULT_LAND);
    }

    @Test
    @Transactional
    void fullUpdateUnternehmenWithPatch() throws Exception {
        // Initialize the database
        unternehmenRepository.saveAndFlush(unternehmen);

        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();

        // Update the unternehmen using partial update
        Unternehmen partialUpdatedUnternehmen = new Unternehmen();
        partialUpdatedUnternehmen.setId(unternehmen.getId());

        partialUpdatedUnternehmen
            .name(UPDATED_NAME)
            .ort(UPDATED_ORT)
            .branche(UPDATED_BRANCHE)
            .beschreibung(UPDATED_BESCHREIBUNG)
            .postleitzahl(UPDATED_POSTLEITZAHL)
            .adresse(UPDATED_ADRESSE)
            .land(UPDATED_LAND);

        restUnternehmenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUnternehmen.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUnternehmen))
            )
            .andExpect(status().isOk());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
        Unternehmen testUnternehmen = unternehmenList.get(unternehmenList.size() - 1);
        assertThat(testUnternehmen.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUnternehmen.getOrt()).isEqualTo(UPDATED_ORT);
        assertThat(testUnternehmen.getBranche()).isEqualTo(UPDATED_BRANCHE);
        assertThat(testUnternehmen.getBeschreibung()).isEqualTo(UPDATED_BESCHREIBUNG);
        assertThat(testUnternehmen.getPostleitzahl()).isEqualTo(UPDATED_POSTLEITZAHL);
        assertThat(testUnternehmen.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testUnternehmen.getLand()).isEqualTo(UPDATED_LAND);
    }

    @Test
    @Transactional
    void patchNonExistingUnternehmen() throws Exception {
        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();
        unternehmen.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUnternehmenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, unternehmen.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(unternehmen))
            )
            .andExpect(status().isBadRequest());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUnternehmen() throws Exception {
        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();
        unternehmen.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnternehmenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(unternehmen))
            )
            .andExpect(status().isBadRequest());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUnternehmen() throws Exception {
        int databaseSizeBeforeUpdate = unternehmenRepository.findAll().size();
        unternehmen.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnternehmenMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(unternehmen))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Unternehmen in the database
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUnternehmen() throws Exception {
        // Initialize the database
        unternehmenRepository.saveAndFlush(unternehmen);

        int databaseSizeBeforeDelete = unternehmenRepository.findAll().size();

        // Delete the unternehmen
        restUnternehmenMockMvc
            .perform(delete(ENTITY_API_URL_ID, unternehmen.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Unternehmen> unternehmenList = unternehmenRepository.findAll();
        assertThat(unternehmenList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
