package praktikumsverwaltung.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import praktikumsverwaltung.web.rest.TestUtil;

class UnternehmenTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Unternehmen.class);
        Unternehmen unternehmen1 = new Unternehmen();
        unternehmen1.setId(1L);
        Unternehmen unternehmen2 = new Unternehmen();
        unternehmen2.setId(unternehmen1.getId());
        assertThat(unternehmen1).isEqualTo(unternehmen2);
        unternehmen2.setId(2L);
        assertThat(unternehmen1).isNotEqualTo(unternehmen2);
        unternehmen1.setId(null);
        assertThat(unternehmen1).isNotEqualTo(unternehmen2);
    }
}
