package praktikumsverwaltung.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import praktikumsverwaltung.web.rest.TestUtil;

class DozentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dozent.class);
        Dozent dozent1 = new Dozent();
        dozent1.setId(1L);
        Dozent dozent2 = new Dozent();
        dozent2.setId(dozent1.getId());
        assertThat(dozent1).isEqualTo(dozent2);
        dozent2.setId(2L);
        assertThat(dozent1).isNotEqualTo(dozent2);
        dozent1.setId(null);
        assertThat(dozent1).isNotEqualTo(dozent2);
    }
}
